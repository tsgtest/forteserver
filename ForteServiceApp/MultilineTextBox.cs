﻿using System;
using System.Text;
using LMP.Architect.Oxml;
using System.Xml.Linq;

namespace TSG.WebControls
{
    public class MultilineTextbox : ForteWebControl
    {
        public MultilineTextbox(XmlVariable segmentVariable, XNamespace namespaceAttr) : base(segmentVariable, namespaceAttr)
        {
            this.NumberOfLines = int.Parse(this.ControlProperties["NumberOfLines"].ToString());
            //this.SelectionMode = (LMP.Controls.TextBox.SelectionTypes)int.Parse(this.ControlProperties["SelectionMode"].ToString());
        }

        public int NumberOfLines { get; set; }
        //public LMP.Controls.TextBox.SelectionTypes SelectionMode { get; set; }
        
        #region ForteWebControl overrides
        public override string ToHtml()
        {
            string ctlVal = this.Value.Replace("\v", "\r\n");
            StringBuilder oSB = new StringBuilder();
            oSB.AppendFormat("<div id='mlt{0}' class='forte-control' data-forte-control-type='MultilineTextbox' data-forte-variable='{1}' data-forte-segment='{2}' style='position: relative; left: {3}; z-index: 1;' xmlns='{6}' >" +
                "<textarea name='MainMultilineTextbox' id='mlTextboxMain{0}' rows='{4}' style='margin-top: 0px; position: relative; top: 0px; left: 0px; width: 100%; resize: none;' xmlns='{6}'> {5} </textarea> </div>",
                Guid.NewGuid().ToString(), this.AssociatedVariableName, this.SegmentName, this.Left == "" ? "0px" : this.Left, this.NumberOfLines, ctlVal, this.Namespace);

            return oSB.ToString();
        }
        #endregion
    }
}
