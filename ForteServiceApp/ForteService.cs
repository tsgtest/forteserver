﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using LMP.Architect.Base;
using LMP.Architect.Oxml;
using LMP.Architect.Oxml.Server;
using DocumentFormat.OpenXml.Wordprocessing;
using LMP.Grail;
using LMP.Data;
using LMP.Controls;
using Newtonsoft.Json;
using System.Xml;
using System.Xml.Linq;

namespace TSG.Forte
{
    #region ************************Enumerations*****************************
    public enum TreeControlTypes
    {
        None = 0,
        JSTree = 1,
        FancyTree = 2
    }
    #endregion

    [ServiceBehaviorAttribute(IncludeExceptionDetailInFaults = true)]
    public class ForteService : IForteService
    {
        #region ************************Enumerations*****************************
        private enum DetailGridFieldTypes
        {
            None = 0,
            TextBox = 1,
            ComboBox = 2,
            ListBox = 3,
            MultiLineCombo = 4,
            BooleanCombo = 5,
            CheckBox = 6
        }
        #endregion

        #region ************************Methods*****************************
        public string CreateDocument(string ID, string prefill, string emailAddress, string xSystemUserID)
        {
            try
            {
                WriteMessageToWindow("CreateDocument() requested from " + emailAddress);

                DateTime t0 = DateTime.Now;

                LMP.Data.Application.Login(xSystemUserID, true);

                //get specified data set from external source
                XmlPrefill oPrefill = new XmlPrefill(prefill, "CurrentPrefill", ID);

                XmlSegment oSegment = XmlSegmentInsertion.Create(ID, null, oPrefill, null, null, null, true, false);

                LMP.Data.Application.Logout();

                byte[] aDoc = new byte[oSegment.Stream.Length];
                oSegment.Stream.Seek(0, SeekOrigin.Begin);
                oSegment.Stream.Read(aDoc, 0, aDoc.Length);

                string xDoc = Convert.ToBase64String(aDoc);
                string xTime = LMP.Benchmarks.ElapsedTime(t0);

                WriteMessageToWindow(string.Format("CreateDocument() request from {0} executed in {1} sec.", emailAddress, xTime));

                return xDoc;
            }
            catch (System.Exception oE)
            {
                WriteMessageToWindow(LMP.Error.GetErrorDetail(oE));
                throw oE;
            }
        }
        public string GetHelpText(string xHelpText, string emailAddress, string xSystemUserID)
        {
            //GLOG 7214: Handle errors in this function
            try
            {

                LMP.Data.Application.Login(xSystemUserID, true);

                //get specified data set from external source
                LMP.Data.FirmApplicationSettings oSettings = new LMP.Data.FirmApplicationSettings(LMP.Data.Application.User.ID);
                int iTextSize = oSettings.HelpFontSize;
                string xTextFont = oSettings.HelpFontName;
                LMP.Data.Application.Logout();
                //set help text or navigate to specified url - 
                //web browser control must
                //be set so that AllowNavigation = true -
                //otherwise, repeated DocumentText assignments fail
                if (string.IsNullOrEmpty(xHelpText))
                {
                    return "";
                }
                else
                {
                    //Text may be html
                    string xDesc = xHelpText.Replace((char)172, '<');
                    xDesc = LMP.String.RestoreMPReservedChars(xDesc);

                    if (xDesc.ToLower().StartsWith(@"file:\\\") ||
                        xDesc.ToLower().StartsWith(@"file:///"))
                    {
                        //remove protocol prefix
                        string xFullName = xDesc.Substring(8);

                        //replace %20 with space - this occurs only on some machines
                        xFullName = xFullName.Replace("%20", " ");

                        //test for existence of file
                        bool bFileExists = false;
                        try
                        {
                            FileInfo oFileInfo = new FileInfo(xFullName);
                            bFileExists = oFileInfo.Exists;
                        }
                        catch { }

                        //assume that the help file is in the help directory
                        //if no directory has been specified
                        if (!bFileExists)
                        {
                            xFullName = LMP.Data.Application.GetDirectory(LMP.Data.mpDirectories.Help) + @"\" + xFullName;
                        }

                        if (File.Exists(xFullName))
                        {
                            //return contents of help file
                            return File.ReadAllText(xFullName);
                        }
                        else
                        {
                            //File name specified doesn't exist
                            xDesc = string.Format("<span style='font-size: {0}pt; font-family: {1}'>" +
                                LMP.Resources.GetLangString("Msg_HelpUnavailable") + "</span>", iTextSize, xTextFont);
                            return xDesc;
                        }
                    }
                    else
                    {
                        xDesc = string.Format("<span style='font-size: {0}pt; font-family: {1}'>" + xDesc + "</span>",
                                                iTextSize,
                                                xTextFont);

                        xDesc = xDesc.Replace("\n", "<br>");

                        //If not a file, just return reformatted string
                        return xDesc;
                    }
                }
            }
            catch (System.Exception oE)
            {
                WriteMessageToWindow(LMP.Error.GetErrorDetail(oE));
                throw oE;
            }
        }
        public string GetListXml(string xListName, string emailAddress, string xSystemUserID)
        {
            const string xListFormat = "<List name=\"{0}\" itemCount=\"{1}\" numCols=\"{2}\" />";
            const string xItemFormat = "<Item index=\"{0}\" col1=\"{1}\" col2=\"{2}\" />";
            try
            {
                WriteMessageToWindow("GetListXml() requested from " + emailAddress);

                DateTime t0 = DateTime.Now;
                LMP.Data.Application.Login(xSystemUserID, true);
                LMP.Data.Lists oLists = LMP.Data.Application.GetLists(false);
                LMP.Data.List oList = (LMP.Data.List)oLists.ItemFromName(xListName);
                ArrayList aListItems = oList.ItemArray;
                int iColCount = oList.ColumnCount;
                int iItemCount = oList.ItemCount;
                LMP.Data.Application.Logout();
                StringBuilder oSB = new StringBuilder();
                oSB.AppendFormat(xListFormat, xListName, iItemCount, iColCount);
                for (int i = 0; i < aListItems.Count; i++)
                {
                    object oItem = aListItems[i];
                    string xCol1Value = ((object[])oItem)[0].ToString();
                    string xCol2Value = "";
                    if (iColCount == 2)
                    {
                        xCol2Value = ((object[])oItem)[1].ToString();
                    }
                    oSB.AppendFormat(xItemFormat, i + 1, xCol1Value, xCol2Value);
                }
                oSB.Append("</List>");
                WriteMessageToWindow(string.Format("GetListXml() request from {0} executed in {1} sec.", emailAddress, LMP.Benchmarks.ElapsedTime(t0)));
                return oSB.ToString();
            }
            catch (System.Exception oE)
            {
                WriteMessageToWindow(LMP.Error.GetErrorDetail(oE));
                throw oE;
            }
        }
        public string GetSegmentTreeXml(string ID, string prefill, string emailAddress, string xSystemUserID)
        {
            try
            {
                WriteMessageToWindow("GetSegmentTreeXml() requested from " + emailAddress);

                DateTime t0 = DateTime.Now;
                LMP.Data.Application.Login(xSystemUserID, true);

                XmlSegment oSegment = null;

                try
                {
                    XmlPrefill oPrefill = new XmlPrefill(prefill, "CurrentPrefill", ID);
                    oSegment = XmlSegmentInsertion.Create(ID, null, oPrefill, null, null, null, true, false);
                }
                finally
                {
                    LMP.Data.Application.Logout();
                }

                StringBuilder oSB = new StringBuilder();
                oSB.Append("<ForteDocument>");
                oSB.Append(GetSegmentUIXml(oSegment));
                oSB.Append("</ForteDocument>");
                WriteMessageToWindow(string.Format("GetSegmentTreeXml() request from {0} executed in {1} sec.", emailAddress, LMP.Benchmarks.ElapsedTime(t0)));
                return oSB.ToString();
            }
            catch (System.Exception oE)
            {
                WriteMessageToWindow(LMP.Error.GetErrorDetail(oE));
                throw oE;
            }
        }
        public string GetExternalDataSet(string id, string emailAddress, string systemUserID)
        {
            DateTime t0 = DateTime.Now;

            //get GrailStore object
            GrailBackend oBackend = LMP.Grail.GrailStore.Backend;

            //connect to GrailBackend - we do this here
            //to avoid the ClientMatter lookup from having to
            //connect to the store again
            oBackend.Connect(null);

            try
            {
                string xPrefill = null;
                xPrefill = oBackend.GetData(id);

                LMP.Data.Application.Login(systemUserID, true);

                try
                {
                    XmlPrefill oPrefill = new XmlPrefill(xPrefill, "DataSet", "0");
                    return oPrefill.ToString();
                }
                finally
                {
                    LMP.Data.Application.Logout();
                }
            }
            finally
            {
                oBackend.Disconnect();

                string xTime = LMP.Benchmarks.ElapsedTime(t0);
                WriteMessageToWindow(string.Format("GetExternalDataSet() request from {0} executed in {1} sec.", emailAddress, xTime));
            }

        }
        public string GetFoldersXml(int iParentFolderID, string emailAddress, string systemUserID)
        {
            try
            {
                WriteMessageToWindow("GetFoldersXml() requested from " + emailAddress);

                DateTime t0 = DateTime.Now;
                System.Collections.ArrayList oList = null;

                LMP.Data.Application.Login(systemUserID, true);

                try
                {
                    LMP.Data.Folders oF = new LMP.Data.Folders(LMP.Data.Application.User.ID, iParentFolderID);
                    oList = oF.ToDisplayArray();
                }
                finally
                {
                    LMP.Data.Application.Logout();
                }


                StringBuilder oSB = new StringBuilder();

                oSB.Append("<Folders>");

                for (int i = 0; i < oList.Count; i++)
                {
                    object[] aListItem = (object[])oList[i];
                    oSB.AppendFormat("<Folder index='{0}' id='{1}' displayName='{2}' />", aListItem[2], aListItem[1], aListItem[0]);
                }

                oSB.Append("</Folders>");


                WriteMessageToWindow(string.Format("CreateDocument() request from {0} executed in {1} sec.", emailAddress, LMP.Benchmarks.ElapsedTime(t0)));

                return oSB.ToString();
            }
            catch (System.Exception oE)
            {
                WriteMessageToWindow(LMP.Error.GetErrorDetail(oE));
                throw oE;
            }
        }
        public string GetFoldersJSON(int iParentFolderID, string emailAddress, string systemUserID)
        {
            try
            {
                WriteMessageToWindow("GetFoldersJSON() requested from " + emailAddress);

                DateTime t0 = DateTime.Now;
                System.Collections.ArrayList oList = null;

                LMP.Data.Application.Login(systemUserID, true);

                try
                {
                    LMP.Data.Folders oF = new LMP.Data.Folders(LMP.Data.Application.User.ID, iParentFolderID);
                    oList = oF.ToDisplayArray();

                    StringBuilder oSB = new StringBuilder();


                    oSB.Append("[");
                    System.Collections.ArrayList oFolders = new System.Collections.ArrayList();

                    for (int i = 0; i < oList.Count; i++)
                    {
                        object[] aListItem = (object[])oList[i];

                        oSB.AppendFormat("[[\"id\" : \"{1}\", \"text\" : \"{0}\"]]", aListItem[0], aListItem[1]);

                        if (i < oList.Count - 1)
                            oSB.Append(",");
                    }
                    oSB.Append("]");

                    oSB.Replace("[[[", "[{");
                    oSB.Replace("[[", "{");
                    oSB.Replace("]]", "}");


                    WriteMessageToWindow(string.Format("CreateDocument() request from {0} executed in {1} sec.", emailAddress, LMP.Benchmarks.ElapsedTime(t0)));

                    string x = oSB.ToString();

                    return x;
                }
                finally
                {
                    LMP.Data.Application.Logout();
                }
            }
            catch (System.Exception oE)
            {
                WriteMessageToWindow(LMP.Error.GetErrorDetail(oE));
                throw oE;
            }
        }
        public string GetFolderMembersJSON(int iParentFolderID, string emailAddress, string systemUserID)
        {
            try
            {
                WriteMessageToWindow("GetFolderMembersJSON() requested from " + emailAddress);

                DateTime t0 = DateTime.Now;
                System.Collections.ArrayList oList = null;

                LMP.Data.Application.Login(systemUserID, true);

                try
                {

                    StringBuilder oSB = new StringBuilder();

                    LMP.Data.Folders oF = new LMP.Data.Folders(LMP.Data.Application.User.ID, iParentFolderID);
                    oList = oF.ToDisplayArray();

                    for (int i = 0; i < oList.Count; i++)
                    {
                        object[] aListItem = (object[])oList[i];

                        oSB.AppendFormat("[[\"id\" : \"{1}\", \"text\" : \"{0}\", \"intendedUse\" : \"0\"]]", aListItem[0], aListItem[1]);

                        if (i < oList.Count - 1)
                            oSB.Append(",");
                    }


                    LMP.Data.FolderMembers oMembers = new LMP.Data.FolderMembers(iParentFolderID);
                    oList = oMembers.ToDisplayArray();

                    for (int i = 0; i < oList.Count; i++)
                    {
                        object[] aListItem = (object[])oList[i];

                        oSB.AppendFormat("[[\"id\" : \"{1}\", \"text\" : \"{0}\", \"intendedUse\" : \"{2}\"]]", aListItem[0], aListItem[1], aListItem[2]);

                        if (i < oList.Count - 1)
                            oSB.Append(",");
                    }

                    oSB.Replace("[[", "{");
                    oSB.Replace("]]", "}");
                    oSB.Insert(0, "[");
                    oSB.Append("]");

                    WriteMessageToWindow(string.Format("CreateDocument() request from {0} executed in {1} sec.", emailAddress, LMP.Benchmarks.ElapsedTime(t0)));

                    string x = oSB.ToString();

                    return x;
                }
                catch (System.Exception oE)
                {
                    string x = oE.Message;
                    return null;
                }
                finally
                {
                    LMP.Data.Application.Logout();
                }
            }
            catch (System.Exception oE)
            {
                WriteMessageToWindow(LMP.Error.GetErrorDetail(oE));
                throw oE;
            }
        }
        #endregion

        #region ************************Private Methods*****************************
        private string GetSegmentUIXml(XmlSegment oSegment)
        {
            const string xSegmentFormat = "<Segment tagId='{0}' name='{1}' displayName='{2}' typeId='{3}' showChooser='{4}' maxAuthors='{5}' authorsNodeUiLabel='{6}' " +
                "showCourtChooser='{7}' l0='{8}' l1='{9}' l2='{10}' l3='{11}' l4='{12}' isTransparent='{13}' linkAuthorsToParent='{14}' >";
            const string xVariableFormat = "<Variable name='{0}' label='{1}' displayValue='{2}' index='{3}' controlType='{4}' validationCondition='{5}' mustVisit='{6}' displayLevel='{7}'>";
            const string xBlockFormat = "<Block name='{0}' label='{1}' displaylevel='{2}' isbody='{3}' />";
            StringBuilder oSB = new StringBuilder();
            oSB.AppendFormat(xSegmentFormat, oSegment.TagID, oSegment.Name, oSegment.DisplayName, oSegment.TypeID.ToString(), oSegment.ShowChooser.ToString(), oSegment.MaxAuthors.ToString(),
                oSegment.AuthorsNodeUILabel, oSegment.ShowCourtChooser.ToString(), oSegment.L0.ToString(), oSegment.L1.ToString(), oSegment.L2.ToString(),
                oSegment.L3.ToString(), oSegment.L4.ToString(), oSegment.IsTransparentDef, oSegment.LinkAuthorsToParentDef);
            if (!string.IsNullOrEmpty(oSegment.HelpText))
            {
                oSB.AppendFormat("<HelpText>{0}</HelpText>", oSegment.HelpText);
            }
            if (!string.IsNullOrEmpty(oSegment.AuthorsHelpText))
            {
                oSB.AppendFormat("<AuthorsHelpText>{0}</AuthorsHelpText>", oSegment.AuthorsHelpText);
            }
            if (!string.IsNullOrEmpty(oSegment.CourtChooserHelpText))
            {
                oSB.AppendFormat("<CourtChooserHelpText>{0}</CourtChooserHelpText>", oSegment.CourtChooserHelpText);
            }
            for (int i = 0; i < oSegment.Variables.Count; i++)
            {
                XmlVariable oVar = oSegment.Variables[i];
                if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) == Variable.ControlHosts.DocumentEditor)
                {
                    oSB.AppendFormat(xVariableFormat, oVar.Name, oVar.DisplayName, oVar.DisplayValue, oVar.ExecutionIndex, oVar.ControlType, oVar.ValidationCondition,
                        oVar.MustVisit, oVar.DisplayLevel);
                    oSB.Append("<Value>" + LMP.String.ReplaceXMLChars(oVar.Value, true) + "</Value>");
                    if (oVar.ControlProperties != "")
                    {
                        oSB.AppendFormat("<Control type=\"{0}\">" + GetControlPropertiesXML(oVar) + "</Control>", oVar.ControlType);
                    }
                    oSB.Append(GetActionsXML(oVar));
                    if (oVar.HelpText != "")
                    {
                        oSB.AppendFormat("<HelpText>{0}</HelpText>", oVar.HelpText);
                    }
                    oSB.Append("</Variable>");
                }
            }
            //JTS: do we need to account for displaying blocks?
            for (int i = 0; i < oSegment.Blocks.Count; i++)
            {
                XmlBlock oBlock = oSegment.Blocks[i];
                if (oBlock.ShowInTree)
                {
                    oSB.AppendFormat(xBlockFormat, oBlock.Name, oBlock.DisplayName, oBlock.DisplayLevel, oBlock.IsBody);
                }
            }
            for (int i = 0; i < oSegment.Segments.Count; i++)
            {
                oSB.Append(GetSegmentUIXml(oSegment.Segments[i]));
            }
            oSB.Append("</Segment>");
            return oSB.ToString();
        }
        private string GetControlPropertiesXML(XmlVariable oVar)
        {
            const string xPropFormat = "<Property name=\"{0}\">{1}</Property>";
            string xCtrlProps = oVar.ControlProperties;
            StringBuilder oSB = new StringBuilder();
            string[] aProps = xCtrlProps.Split('†');
            for (int i = 0; i <= aProps.GetUpperBound(0); i++)
            {
                int iPos = aProps[i].IndexOf('=');
                if (iPos > -1)
                {
                    string xPropName = aProps[i].Substring(0, iPos);
                    string xPropVal = XmlExpression.Evaluate(aProps[i].Substring(iPos + 1), oVar.Segment, oVar.ForteDocument);
                    if (oVar.ControlType == LMP.Data.mpControlTypes.MultilineCombo && xPropName.ToLower() == "separator")
                    {
                        //Special cases for multi-line combo separators
                        switch (xPropVal)
                        {
                            case "1310":
                                xPropVal = "[Char__13][Char__10]";
                                break;
                            case "13":
                                xPropVal = "[Char__13]";
                                break;
                            case "11":
                                xPropVal = "[Char__11]";
                                break;
                            case "9":
                                xPropVal = "[Char__9]";
                                break;
                        }
                    }
                    xPropVal = GetControlPropertyFieldsXML(oVar.ControlType, xPropName, xPropVal);
                    oSB.AppendFormat(xPropFormat, xPropName, xPropVal);
                }
            }
            return oSB.ToString();
        }
        private string GetControlPropertyFieldsXML(LMP.Data.mpControlTypes iType, string xPropName, string xPropVal)
        {
            const string DETAIL_SHARED_FORMAT = "<Detail controlType='{0}' fieldName='{1}' displayName='{2}' ciEnabled='{3}'{4} />";
            xPropVal = LMP.String.ReplaceXMLChars(xPropVal, true);
            string xRetVal = xPropVal;
            if (iType == LMP.Data.mpControlTypes.DetailGrid && xPropName.ToLower() == "configstring")
            {
                xRetVal = "";
                string[] aVals = xPropVal.Split('‡');
                int iC = 0;
                while (iC < aVals.GetUpperBound(0))
                {
                    DetailGridFieldTypes iDetailCtl = (DetailGridFieldTypes)Int32.Parse(aVals[0 + iC]);
                    string xExtraFormat = "";
                    switch (iDetailCtl)
                    {
                        case DetailGridFieldTypes.TextBox:
                        case DetailGridFieldTypes.CheckBox:
                            xExtraFormat = " lines='" + aVals[iC] + "'";
                            break;
                        case DetailGridFieldTypes.MultiLineCombo:
                        case DetailGridFieldTypes.ListBox:
                        case DetailGridFieldTypes.ComboBox:
                            xExtraFormat = " listname='" + aVals[4 + iC] + "'";
                            break;
                        default:
                            break;
                    }
                    xRetVal += string.Format(DETAIL_SHARED_FORMAT, iDetailCtl.ToString(), aVals[iC + 1], aVals[iC + 2], aVals[iC + 3], xExtraFormat);
                    iC += 5;
                }
            }
            return xRetVal;
        }
        private string[] GetVariableActionParameters(XmlVariableActions.Types iActionType)
        {
            switch (iActionType)
            {
                case XmlVariableActions.Types.SetVariableValue:
                    return new string[] {"VariableName",
                                            "Expression"};
                case XmlVariableActions.Types.RunVariableActions:
                    return new string[] { "VariableNames" };
                default:
                    return null;
            }
        }
        private string[] GetControlActionParameters(XmlControlActions.Types iActionType)
        {
            switch (iActionType)
            {
                case XmlControlActions.Types.ChangeLabel:
                    return new string[] { "VariableName",
                                          "NewLabel"};
                case XmlControlActions.Types.Enable:
                    return new string[] { "VariableName",
                                          "Expression"};
                case XmlControlActions.Types.SetVariableValue:
                    return new string[] { "VariableName",
                                          "Expression" };
                case XmlControlActions.Types.Visible:
                    return new string[]  { "VariableNames",
                                           "Expression"};
                case XmlControlActions.Types.DisplayMessage:
                    return new string[] { "MessageText",
                                          "Caption"};
                default:
                    return null;
            }
        }
        private string GetActionsXML(XmlVariable oVar)
        {
            const string xActionStartFormat = "<Action event='{0}' type='{1}' >";
            const string xActionEnd = "</Action>";

            StringBuilder oSB = new StringBuilder();
            for (int i = 0; i < oVar.ControlActions.Count; i++)
            {
                XmlControlAction oCtlAct = oVar.ControlActions[i];
                switch (oCtlAct.Type)
                {
                    case XmlControlActions.Types.ChangeLabel:
                    case XmlControlActions.Types.DisplayMessage:
                    case XmlControlActions.Types.Enable:
                    case XmlControlActions.Types.Visible:
                    case XmlControlActions.Types.EndExecution:
                    case XmlControlActions.Types.SetVariableValue:
                        oSB.AppendFormat(xActionStartFormat, oCtlAct.Event, oCtlAct.Type);
                        if (oCtlAct.ExecutionCondition != "")
                        {
                            oSB.AppendFormat("<ExecCondition>" + LMP.String.ReplaceXMLChars(oCtlAct.ExecutionCondition, true) + "</ExecCondition>");
                        }
                        oSB.Append(GetControlActionParametersXml(oCtlAct));
                        oSB.Append(xActionEnd);
                        break;
                    default:
                        //Only include actions that affect display of other controls
                        break;
                }
            }
            for (int i = 0; i < oVar.VariableActions.Count; i++)
            {
                XmlVariableAction oVarAct = oVar.VariableActions[i];
                switch (oVarAct.Type)
                {
                    case XmlVariableActions.Types.EndExecution:
                    case XmlVariableActions.Types.RunVariableActions:
                    case XmlVariableActions.Types.SetVariableValue:
                        oSB.AppendFormat(xActionStartFormat, "ValueChanged", oVarAct.Type);
                        if (oVarAct.ExecutionCondition != "")
                        {
                            oSB.AppendFormat("<ExecCondition>" + LMP.String.ReplaceXMLChars(oVarAct.ExecutionCondition, true) + "</ExecCondition>");
                        }
                        if (oVarAct.LookupListID > 0)
                        {
                            oSB.Append("<MappingListID>" + oVarAct.LookupListID + "</MappingListID>");
                        }
                        oSB.Append(GetVariableActionParametersXml(oVarAct));
                        oSB.Append(xActionEnd);
                        break;
                    default:
                        //only include actions that might affect value of other variables
                        break;
                }
            }
            if (oSB.ToString() != "")
            {
                oSB.Insert(0, "<Actions>");
                oSB.Append("</Actions>");
            }
            return oSB.ToString();
        }
        private string GetVariableActionParametersXml(XmlVariableAction oAction)
        {
            string xParam = LMP.String.ReplaceXMLChars(oAction.Parameters, true);
            string xParamXml = "";
            string[] aParamNames = GetVariableActionParameters(oAction.Type);
            if (aParamNames != null)
            {
                string[] aParamVals = xParam.Split('‡');
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    xParamXml += "<" + aParamNames[i] + ">" + aParamVals[i] + "</" + aParamNames[i] + ">";
                }
            }
            if (xParamXml != "")
            {
                xParamXml = "<Parameters>" + xParamXml + "</Parameters>";
            }
            return xParamXml;
        }
        private string GetControlActionParametersXml(XmlControlAction oAction)
        {
            string xParam = LMP.String.ReplaceXMLChars(oAction.Parameters, true);
            string xParamXml = "";
            string[] aParamNames = GetControlActionParameters(oAction.Type);
            if (aParamNames != null)
            {
                string[] aParamVals = xParam.Split('‡');
                for (int i = 0; i <= aParamNames.GetUpperBound(0); i++)
                {
                    xParamXml += "<" + aParamNames[i] + ">" + aParamVals[i] + "</" + aParamNames[i] + ">";
                }
            }
            if (xParamXml != "")
            {
                xParamXml = "<Parameters>" + xParamXml + "</Parameters>";
            }
            return xParamXml;
        }
        #endregion

        public static void WriteMessageToWindow(string msg)
        {
            ForteServiceApp.Program.forteServiceForm.WriteMessage(msg);
        }
        public string GetSegmentTreeHtml(string ID, string prefill, TreeControlTypes treeCtlType, string emailAddress, string sysUserID)
        {
            DateTime t0 = DateTime.Now;
            string uIHtml = null;

            WriteMessageToWindow("GetSegmentTreeHtml() requested from " + emailAddress);

            try
            {
                SegmentTree oTree = new SegmentTree(ID, prefill, treeCtlType, sysUserID);

                //return string
                uIHtml = oTree.ToHtml();

                return uIHtml;
            }
            finally
            {
                WriteMessageToWindow(string.Format("GetSegmentTreeHtml() request from {0} executed in {1} sec.", emailAddress, LMP.Benchmarks.ElapsedTime(t0)));
            }
        }
    }
}
