﻿using System;
using System.Text;
using LMP.Architect.Oxml;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using System.Xml.Linq;

namespace TSG.WebControls
{
    public class DetailGrid : ForteWebControl
    {
        private const string DEFAULT_CONFIGSTRING = "1‡FULLNAME‡&Name:‡True‡‡1‡COREADDRESS‡&Address:‡True‡4";
        private const int MAX_DETAIL_FIELDS = 12;
        private int m_iCurIndex = 0;
        private List<List<string>> m_ValueArray = new List<List<string>>();
        private List<DetailControl> m_Controls = new List<DetailControl>();

        private struct DetailControl
        {
            public string DisplayText;
            public LMP.Controls.Detail.mpDetailControlTypes ControlType;
            public string FieldName;
            public string ListName;
            public int Lines;
            public bool CIEnabled;
        }

        public DetailGrid(XmlVariable segmentVariable, XNamespace namespaceAttr ) : base(segmentVariable, namespaceAttr)
        {
            this.ConfigString = this.ControlProperties["ConfigString"].ToString();
            this.CounterScheme = this.ControlProperties["CounterScheme"].ToString();
            object oMaxEntities = this.ControlProperties["MaxEntities"];
            if (oMaxEntities != null)
            {
                this.MaxEntities = int.Parse(oMaxEntities.ToString());
            }
            object oColWidth = this.ControlProperties["LabelColumnWidth"];
            if (oColWidth != null)
            {
                this.LabelColumnWidth = int.Parse(oColWidth.ToString());
            }
            else
                this.LabelColumnWidth = 85;

            this.LoadArray(segmentVariable.Value);
        }

        public string CounterScheme { get; set; }
        public int MaxEntities { get; set; }
        public int LabelColumnWidth { get; set; }


        #region ForteWebControl overrides
        public override string ToHtml()
        {
            string ctlVal = this.Value.Replace("\v", "\r\n");
            StringBuilder oSB = new StringBuilder();
            string xGuid = Guid.NewGuid().ToString();
            string xControlsHtml = GetControlsHtml(m_iCurIndex, xGuid);
            oSB.AppendFormat("<div id='dtg{0}' class='forte-control' data-forte-control-type='DetailGrid' data-forte-variable='{1}' data-forte-segment='{2}' data-detail-index='{3}' " +
                "data-detail-counterscheme='{4}' data-detail-list='{5}' xmlns='{6}' >" +
                "{7} </div>",
                xGuid, this.AssociatedVariableName, this.SegmentName, m_iCurIndex, this.CounterScheme,
                new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(m_ValueArray), this.Namespace, xControlsHtml);

            return oSB.ToString();
        }
        public void MovePrev()
        {
            System.Windows.Forms.MessageBox.Show("Move to Previous Detail");
        }
        public void MoveNext()
        {
            System.Windows.Forms.MessageBox.Show("Move to Next Detail");
        }
        #endregion
        private string GetControlsHtml(int iDetail, string xGuid)
        {
            StringBuilder oSB = new StringBuilder();
            List<string> aVals =  m_ValueArray[iDetail];
            for (int i = 0; i < m_Controls.Count; i++)
            {
                string xLabel = m_Controls[i].DisplayText.Replace("&", "");
                oSB.AppendFormat("<label class='detail-grid-control-label' id='lbl{0}_{1}' style='display:inline-block;width:{2}px;vertical-align:top' xmlns='{4}' >{3}</label>", i + 1, xGuid, this.LabelColumnWidth, xLabel, this.Namespace);
                string xVal = aVals[i+1];
                switch (m_Controls[i].ControlType)
                {
                    case LMP.Controls.Detail.mpDetailControlTypes.TextBox:
                        if (m_Controls[i].Lines > 1)
                        {
                            oSB.AppendFormat("<textarea rows='{0}' class='detail-grid-control-text' id='detail{1}_{2}' xmlns='{3}' style='resize:none;' >{4}</textarea>", m_Controls[i].Lines, i + 1, xGuid, this.Namespace, xVal);
                        }
                        else
                        {
                            oSB.AppendFormat("<input type='text' class='detail-grid-control-input' id='detail{0}_{1}' value='{2}' xmlns='{3}' />", i + 1, xGuid, xVal, this.Namespace);
                        }
                        break;
                    default:
                        oSB.AppendFormat("<input type='text' class='detail-grid-control-input' id='detail{0}_{1}' value='{2}' xmlns='{3}' />", i + 1, xGuid, xVal, this.Namespace);
                        break;
                }
                oSB.Append("<br/>");
            }
            oSB.AppendFormat("<input type='button' class='detail-grid-control-button-prev' value='&lt;' id='btnPrev{0}' xmlns='{1}' style='width:20px;left:0%' onclick='DetailGridMovePrev(this.parentElement)' />", 
                xGuid, this.Namespace);
            oSB.AppendFormat("<label id='lblItem{0}' style='width:80%' xmlns='{3}' >Item {1} of {2}</label>", xGuid, iDetail + 1, m_ValueArray.Count, this.Namespace);
            oSB.AppendFormat("<input type='button' class='detail-grid-control-button-next' value='>' id='btnNext{0}' xmlns='{1}' style='width:20px;right:0%' onclick='DetailGridMoveNext(this.parentElement)'/>", 
                xGuid, this.Namespace);

            return oSB.ToString();
        }
        private string ConfigString
        {
            set
            {
                //value cannot be empty
                if (value == "")
                {
                    value = DEFAULT_CONFIGSTRING;
                }
                string[] aConfig = value.Split(LMP.StringArray.mpEndOfSubField);

                if ((aConfig.GetLength(0) % 5 != 0) || (aConfig.GetLength(0) % 5 > MAX_DETAIL_FIELDS))
                {
                    //wrong number of array elements - alert
                    throw new LMP.Exceptions.DataException(
                        LMP.Resources.GetLangString("Error_InvalidDetailConfigString") +
                        value);
                }

                for (int i = 0; i < aConfig.GetLength(0); i++)
                {
                    DetailControl oControl = new DetailControl();
                    oControl.ControlType = (LMP.Controls.Detail.mpDetailControlTypes)Int32.Parse(aConfig[i++]);
                    oControl.FieldName = aConfig[i++];
                    oControl.DisplayText = aConfig[i++];
                    oControl.CIEnabled = (aConfig[i++].ToUpper() == "TRUE");
                    switch (oControl.ControlType)
                    {
                        case LMP.Controls.Detail.mpDetailControlTypes.ComboBox:
                        case  LMP.Controls.Detail.mpDetailControlTypes.ListBox:
                        case LMP.Controls.Detail.mpDetailControlTypes.MultilineCombo:
                            oControl.ListName = aConfig[i];
                            oControl.Lines = 1;
                            break;
                        case LMP.Controls.Detail.mpDetailControlTypes.TextBox:
                            oControl.Lines = Int32.Parse(aConfig[i]);
                            oControl.ListName = "";
                            break;
                        default:
                            oControl.Lines = 1;
                            oControl.ListName = "";
                            break;
                    }
                    m_Controls.Add(oControl);
                }
            }
        }

        private void LoadArray(string xValue)
        {
            //Extract just XML portion of string
            int iStart = xValue.IndexOf("<");
            string xContentString = "";
            if (iStart > -1)
            {
                int iEnd = xValue.LastIndexOf(">");
                if (iEnd > -1)
                {
                    xContentString = xValue.Substring(iStart, (iEnd + 1) - iStart);
                }
            }

            if (xContentString != "")
            {

                //repopulate detail array
                XmlDocument oXML = new System.Xml.XmlDocument();
                oXML.LoadXml(string.Concat("<zzmpD>", xContentString, "</zzmpD>"));

                XmlNodeList oNodeList;
                int iIndex = 0;
                do
                {
                    iIndex++;
                    oNodeList = oXML.DocumentElement.SelectNodes(
                        "(/zzmpD/*|/zzmpD/*)[@Index='" + iIndex.ToString() + "']");
                    if (oNodeList.Count > 0)
                    {
                        List<string> aVals = new List<string>(m_Controls.Count + 1);
                        for (int c = 0; c <= m_Controls.Count; c++)
                        {
                            aVals.Add("");
                        }

                        //get UNID, if one exists
                        string xUNID = "";
                        try
                        {
                            xUNID = oNodeList[0].Attributes.GetNamedItem("UNID").Value;
                        }
                        catch { }

                        aVals[0] = xUNID != "" ? xUNID : "0";

                        string xItem = "";

                        for (int i = 0; i < oNodeList.Count; i++)
                        {
                            string xName = oNodeList[i].Name;
                            string xVal = oNodeList[i].InnerText;
                            int iFieldIndex = -1; //GLOG 7895

                            //GLOG : 5686 : CEH
                            //if FieldCount is 1, then all node values go into one field
                            if (m_Controls.Count == 1)
                            {
                                if (xVal != "")
                                {
                                    xItem = xItem != "" ? aVals[1] + "\r\n" + xVal : xVal;
                                    aVals[1] = xItem;
                                }
                            }
                            else
                            {
                                //GLOG 7895: There may be more fields in saved data than are defined for control
                                if (i < m_Controls.Count && m_Controls[i].FieldName.ToLower() == xName.ToLower())
                                    iFieldIndex = i + 1;
                                else
                                {
                                    for (int j = 0; j < m_Controls.Count; j++)
                                    {
                                        if (m_Controls[j].FieldName.ToLower() == xName.ToLower())
                                        {
                                            iFieldIndex = j + 1;
                                            break;
                                        }
                                    }
                                }
                                //GLOG 7895: If there's no match for field name in Detail,
                                //we don't want to add to array
                                if (iFieldIndex > -1)
                                    aVals[iFieldIndex] = xVal;
                            }
                        }

                        m_ValueArray.Add(aVals);
                    }
                } while (oNodeList.Count > 0);

            }
        }

    }
}
