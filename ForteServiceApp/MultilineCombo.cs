﻿using System;
using System.Text;
using LMP.Architect.Oxml;
using System.Xml.Linq;

namespace TSG.WebControls
{
    public class MultilineCombo: ForteWebComboBase
    {
        public MultilineCombo(XmlVariable segmentVariable, XNamespace namespaceAttr) : base(segmentVariable, namespaceAttr)
        {
            this.ForceCaps = bool.Parse(this.ControlProperties["ForceCaps"].ToString());
            this.ListName = this.ControlProperties["ListName"].ToString();
            this.ListRows = int.Parse(this.ControlProperties["ListRows"].ToString());
            this.Separator = this.ControlProperties["Separator"].ToString();
            this.Height = this.ControlProperties["Height"].ToString();
        }

        public bool ForceCaps { get; set; }
        public string Separator { get; set; }
        public bool ShowBorder { get; set; }

        #region ForteWebControl overrides
        public override string ToHtml()
        {
        StringBuilder oSB=new StringBuilder();
        oSB.AppendFormat("<div id='mlc{0}' class='forte-control' data-forte-control-type='MultilineCombo' data-forte-variable='{1}' data-forte-segment='{2}' style='height:{4}px' xmlns='{8}' >" +
            "<textarea name='MainTextbox' class='forte-control-mlc-textarea' id='txtMain{0}' accesskey='a' draggable='false' xmlns='{8}' >{5}</textarea>" +
            "<input type='button' class='forte-control-mlc-button' name='DropDownButton' value='v' id='btnDD{0}' onclick='MlcToggleDropdown(this.parentElement)' xmlns='{8}' />" +
            "<select name='MainListbox' class='forte-control-mlc-select' id='lstMain{0}' size='{6}' onclick='MlcAddSelection(this.parentElement)' onblur='MlcCloseDropdown(this.parentElement)' " +
            "style='margin-top: 0px; position:absolute; top: 100%; left: 0px; display: none' xmlns='{8}'> {7} </select > </div >",
            Guid.NewGuid().ToString(), this.AssociatedVariableName, this.SegmentName, this.Left == "" ? "0px" : this.Left, this.Height, this.Value, 
                this.ListRows, GetListItemHtml(this.ListName), this.Namespace);

            return oSB.ToString();
        }
        #endregion
    }
}
