﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Collections.Specialized;
using LMP.Architect.Base;
using LMP.Architect.Oxml;
using LMP.Architect.Oxml.Server;
using LMP.Grail;

namespace TSG.WebControls
{
    public abstract class ForteWebControl
    {
        #region ************************fields*****************************
        protected XmlVariable segmentVariable;
        #endregion

        #region ************************Constructors*****************************
        public ForteWebControl(XmlVariable segmentVariable, XNamespace namespaceAttr)
        {
            this.segmentVariable = segmentVariable;
            this.Namespace = namespaceAttr;
            string controlProps = segmentVariable.ControlProperties;
            this.ControlProperties = GetControlPropertiesDic(controlProps);


            this.Left = this.ControlProperties["Left"];
            if (string.IsNullOrEmpty(this.Left))
                this.Left = "0px";

            this.Height = this.ControlProperties["Height"];
            if (string.IsNullOrEmpty(this.Height))
                this.Height = "22px";

            this.AssociatedVariableName = segmentVariable.Name;
            this.SegmentName = segmentVariable.SegmentName;
            this.Value = segmentVariable.Value.Replace('\v', '\r');
        }
        #endregion

        #region ************************Properties*****************************
        protected NameValueCollection ControlProperties { get; private set; }
        public string Left { protected get; set; }
        public string Height { protected get; set; }
        public string Value { protected get; set; }
        public string AssociatedVariableName { protected get; set; }
        public string SegmentName { protected get; set; }
        public XNamespace Namespace { protected get; set; }
        #endregion

        #region ************************Methods*****************************
        public abstract string ToHtml();

        public XElement XElement
        {
            get
            {
                return XElement.Parse(this.ToHtml());
            }
        }
        private NameValueCollection GetControlPropertiesDic(string controlProperties)
        {
            if (!string.IsNullOrEmpty(controlProperties))
            {
                NameValueCollection collection = new NameValueCollection();
                string[] ctlPropArray = controlProperties.Split('†');
                foreach (string prop in ctlPropArray)
                {
                    string[] propArray = prop.Split('=');
                    collection.Add(propArray[0], propArray[1]);
                }
                return collection;
            }
            else
                return null;
        }
        #endregion
    }
}
