﻿using System;
using System.Text;
using LMP.Architect.Oxml;
using System.Xml.Linq;

namespace TSG.WebControls
{
    public class CheckBox : ForteWebControl
    {
        public CheckBox(XmlVariable segmentVariable, XNamespace namespaceAttr) : base(segmentVariable, namespaceAttr)
        {
            this.TrueText = "Yes";
            this.FalseText = "No";
            string xProps = this.segmentVariable.ControlProperties;
            string[] aProps = this.segmentVariable.ControlProperties.Split(LMP.StringArray.mpEndOfSubValue);

            foreach (string xProp in aProps)
            {
                if (xProp.ToLower().StartsWith("truestring"))
                {
                    string[] aVal = xProp.Split('=');
                    this.TrueText = XmlExpression.Evaluate(aVal[1], this.segmentVariable.Segment, this.segmentVariable.Segment.ForteDocument);
                }
                else if (xProp.ToLower().StartsWith("falsestring"))
                {
                    string[] aVal = xProp.Split('=');
                    this.FalseText = XmlExpression.Evaluate(aVal[1], this.segmentVariable.Segment, this.segmentVariable.Segment.ForteDocument);
                }
            }
        }

        public override string ToHtml()
        {
            StringBuilder oSB = new StringBuilder();
            oSB.AppendFormat("<div id='chk{0}' class='forte-control' data-forte-control-type='CheckBox' data-forte-variable='{1}' data-forte-segment='{2}' style='position: relative; height: {3}; z - index: 1;'>" +
                "<select name='MainCheckBox' id='chkMain{0}' style='margin-top: 0px; position:absolute; top: 0px; width: 100%;' xmlns='{5}' > {4} </select > </div >",
                Guid.NewGuid().ToString(), this.AssociatedVariableName, this.SegmentName, this.Height, this.GetOptions(), this.Namespace);

            return oSB.ToString();
        }
        public string TrueText { get; set; }
        public string FalseText { get; set; }

        #region ************************Methods*****************************
        protected string GetOptions()
        {
            StringBuilder sb = new StringBuilder();

            XElement trueOption = new XElement (this.Namespace + "option", new XAttribute("value", "true"),
                this.Value.ToLower() == "true" ? new XAttribute(this.Namespace + "selected", "true") : null)
            { Value = this.TrueText };

            sb.AppendLine(trueOption.ToString());

            XElement falseOption = new XElement(this.Namespace + "option", new XAttribute("value", "false"),
                this.Value.ToLower() != "true" ? new XAttribute(this.Namespace + "selected", "true") : null)
            { Value = this.FalseText };

            sb.AppendLine(falseOption.ToString());

            return sb.ToString();
        }

        #endregion

    }
}
