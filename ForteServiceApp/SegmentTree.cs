﻿using System;
using System.Text;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using LMP.Controls;
using LMP.Architect.Base;
using LMP.Architect.Oxml;
using LMP.Architect.Oxml.Server;
using DocumentFormat.OpenXml.Wordprocessing;

namespace TSG.Forte
{
    internal class SegmentTree
    {
        #region ************************Properties*****************************
        private string SegmentID { get; set; }
        private string Prefill { get; set; }
        private TreeControlTypes TreeControlType { get; set; }
        private string SystemUserID { get; set; }
        #endregion

        #region ************************Constructors*****************************
        public SegmentTree(string segmentID, string prefill, TreeControlTypes treeCtlType, string systemUserID)
        {
            this.SegmentID = segmentID;
            this.Prefill = prefill;
            this.TreeControlType = treeCtlType;
            this.SystemUserID = systemUserID;
        }
        #endregion

        #region ************************Methods*****************************
        public string ToHtml()
        {
            XNamespace xNS = @"http://www.w3.org/1999/xhtml";
            XElement oSegTree = null;

            //log in to session
            LMP.Data.Application.Login(this.SystemUserID, true);

            try
            {
                XmlPrefill oPrefill = new XmlPrefill(this.Prefill, "CurrentPrefill", this.SegmentID);
                XmlSegment oSegment = XmlSegmentInsertion.Create(this.SegmentID, null, oPrefill, null, null, null, true, false);

                //get segment tree html element and children
                oSegTree = GetSegmentTreeElement(oSegment, xNS, true, true, this.TreeControlType);
            }
            finally
            {
                LMP.Data.Application.Logout();
            }

            //add top-level ul element
            var xul = new XElement(xNS + "ul", new XAttribute("class", "forte-segments"));

            //add segment tree elements to html
            xul.Add(oSegTree);

            //add header to ui html
            XElement oHead = new XElement(xNS + "head",
                   new XElement(xNS + "meta", new XAttribute("http-equiv", "content-type"), new XAttribute("content", "text/html; charset=ISO-8859-1")),
                   new XElement(xNS + "link", new XAttribute("rel", "stylesheet")));

            ////add script functions to ui html
            //string scriptText = File.ReadAllText("Forte.js");
            //XElement oScript = new XElement(xNS + "script", scriptText);

            switch (this.TreeControlType)
            {
                case TreeControlTypes.FancyTree:
                    oHead.Add(GetFancyTreeHeaderElements(xNS));
                    break;
                case TreeControlTypes.JSTree:
                    oHead.Add(GetJsTreeHeaderElements(xNS));
                    break;
                default:
                    oHead.Add(GetHtmlHeaderElements(xNS));
                    break;
            }

            string xJSFile = "forte.js";
            if (this.TreeControlType == TreeControlTypes.JSTree)
            {
                xJSFile = "forte-jstree.js";
            }
            else if (this.TreeControlType == TreeControlTypes.FancyTree)
            {
                xJSFile = "forte-ftree.js";
            }
            //JTS 8/16/17: Script elements in header can't be self-closing
            XElement oScript = new XElement(xNS + "script", new XAttribute("src", xJSFile)) { Value = "" };
            oHead.Add(oScript);
            oScript = new XElement(xNS + "script", new XAttribute("src", "forte-controls.js")) { Value = "" };
            oHead.Add(oScript);

            oHead.Add(GetHeaderStylesElement(this.TreeControlType, xNS));

            //create body element, putting segment tree in a table cell
            XElement oBody = new XElement(xNS + "body", new XAttribute("onload", "OnLoadHandler()"),
                   new XElement(xNS + "table", new XAttribute("width", "90%"), new XAttribute("cellpadding", "0"), new XAttribute("border", "0"), new XAttribute("class", "forte-segment-input-UI"),
                       new XElement(xNS + "tr",
                           new XElement(xNS + "td",
                           new XElement(xNS + "div", new XAttribute("id", "forte-segment-tree"), xul)))));

            //create new xml document
            //var xDocument = new XDocument(
            //    new XDocumentType("html", null, null, null),
            //        new XElement(xNS + "html", oHead, oBody, treeCtlType == TreeControlTypes.None ? GetHtmlScriptElement(xNS) : null));

            var xDocument = new XDocument(
                new XDocumentType("html", null, null, null),
                    new XElement(xNS + "html", oHead, oBody));

            //write to a string builder
            var settings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                IndentChars = "   ",
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };

            StringBuilder oSB = new StringBuilder();
            using (var writer = XmlWriter.Create(oSB, settings))
            {
                xDocument.WriteTo(writer);
            }

            return oSB.ToString().Replace("&gt;", ">");
        }
        #endregion

        #region ************************Private Methods*****************************
        private XElement GetSegmentTreeElement(XmlSegment oSegment, XNamespace xNS, bool bIncludeChildren, bool bTopLevel, TreeControlTypes treeControl)
        {
            XAttribute dataAttr = null;
            XAttribute moreAttr = null;
            string jsTreeOpenClass = "";

            switch (treeControl)
            {
                case TreeControlTypes.JSTree:
                    dataAttr = new XAttribute("data-jstree", "{\"icon\":" + (bTopLevel ? "\"SegmentTransparent.png\"" : "\"SegmentComponentTransparent.png\"") + "}");
                    moreAttr = new XAttribute("data-jstree", "{\"icon\": \"MoreNode.png\"}");
                    jsTreeOpenClass = " jstree-open";
                    break;
                case TreeControlTypes.FancyTree:
                    dataAttr = new XAttribute("data-icon", (bTopLevel ? "SegmentTransparent.png" : "SegmentComponentTransparent.png"));
                    moreAttr = new XAttribute("data-icon", "MoreNode.png");
                    jsTreeOpenClass = " expanded";
                    break;
                default:
                    dataAttr = null;
                    break;
            }
            if (!oSegment.IsTopLevel && oSegment.IsTransparent && oSegment is XmlCollectionTable && oSegment.Segments.Count == 1)
            {
                oSegment = oSegment.Segments[0];
            }
            if (oSegment.IsTopLevel || !oSegment.IsTransparent || (oSegment is XmlCollectionTable && oSegment.Segments.Count > 1))
            {
                //get segment list item element
                XElement segmentElement = new XElement(xNS + "li", new XAttribute("class", (bTopLevel ? "forte-segment" + jsTreeOpenClass : "forte-child-segment")),
                    new XAttribute("id", oSegment.TagID), dataAttr, new XElement(xNS + "a", new XAttribute("class", "forte-segment-display-name")) { Value = oSegment.DisplayName });

                //XElement segmentElement = new XElement(xNS + "li", new XAttribute("class", "forte-segment" + (bTopLevel ? "-top-level expanded" + jsTreeOpenClass : "")),
                //    new XAttribute("id", oSegment.TagID), dataAttr, new XElement(xNS + "a", new XAttribute("class", "forte-segment-display-name")) { Value = oSegment.DisplayName });

                //create variables element
                XElement variablesListElement = new XElement(xNS + "ul", new XAttribute("class", "forte-variables"));
                XElement moreListElement = new XElement(xNS + "ul", new XAttribute("class", "forte-more-variables"));
                //cycle through segment vars, adding ui elements for each
                for (int i = 0; i < oSegment.Variables.Count; i++)
                {
                    XmlVariable oVar = oSegment.Variables[i];

                    //include only if specified to be displayed in doc editor
                    if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) == Variable.ControlHosts.DocumentEditor)
                    {
                        if (oVar.DisplayLevel == Variable.Levels.Advanced)
                            moreListElement.Add(GetVariableTreeElement(oVar, xNS, true, treeControl));
                        else
                            variablesListElement.Add(GetVariableTreeElement(oVar, xNS, true, treeControl));
                    }
                }
                AddTransparentSegmentVariables(variablesListElement, oSegment, xNS, treeControl);
                if (moreListElement.FirstNode != null)
                {
                    XElement moreTopElement = new XElement(xNS + "li", new XAttribute("class", "forte-more-node"), new XAttribute("id", oSegment.TagID + "_More"), moreAttr,
                        new XElement(xNS + "a", new XAttribute("class", "forte-more-display-value")) { Value = "More..." });
                    moreTopElement.Add(moreListElement);
                    variablesListElement.Add(moreTopElement);
                }
                //recurse for child segments
                for (int i = 0; i < oSegment.Segments.Count; i++)
                {
                    variablesListElement.Add(GetSegmentTreeElement(oSegment.Segments[i], xNS, bIncludeChildren, false, treeControl));
                }

                //add variable elements if they exist
                if (variablesListElement.FirstNode != null)
                {
                    segmentElement.Add(variablesListElement);
                }
                return segmentElement;
            }
            else
            {
                return null;
            }
        }
        private void AddTransparentSegmentVariables(XElement oVarElement, XmlSegment oParent, XNamespace xNS, TreeControlTypes treeControl)
        {
            for (int i = 0; i < oParent.Segments.Count; i++)
            {
                XmlSegment oChild = oParent.Segments[i];
                if (oChild.IsTransparent && oChild is XmlCollectionTable)
                {
                    if (oChild.Segments.Count > 1)
                        continue;
                    else
                        AddTransparentSegmentVariables(oVarElement, oChild, xNS, treeControl);
                }
                else if (oChild.IsTransparent)
                {
                    //cycle through segment vars, adding ui elements for each
                    for (int v = 0; v < oChild.Variables.Count; v++)
                    {
                        XmlVariable oVar = oChild.Variables[v];

                        //include only if specified to be displayed in doc editor
                        if ((oVar.DisplayIn & Variable.ControlHosts.DocumentEditor) == Variable.ControlHosts.DocumentEditor)
                        {
                            oVarElement.Add(GetVariableTreeElement(oVar, xNS, true, treeControl));
                        }
                    }
                }
            }
        }
        /// <summary>
        /// returns the html for the specified variable in the format for the specified tree control
        /// </summary>
        /// <param name="forteVariable"></param>
        /// <param name="xNS"></param>
        /// <param name="bShowValue"></param>
        /// <param name="treeControl"></param>
        /// <returns></returns>
        private XElement GetVariableTreeElement(XmlVariable forteVariable, XNamespace xNS, bool bShowValue, TreeControlTypes treeControl)
        {
            XAttribute dataAttr = null;
            string xJSTreeClass = "";

            switch (treeControl)
            {
                case TreeControlTypes.JSTree:
                    dataAttr = new XAttribute("data-jstree", "{\"icon\": \"VariableValid.png\"}");
                    xJSTreeClass = " jstree-open";
                    break;
                case TreeControlTypes.FancyTree:
                    dataAttr = new XAttribute("data-icon", "VariableValid.png");
                    xJSTreeClass = " expanded";
                    break;
                default:
                    dataAttr = null;
                    break;
            }

            //get variable list item element
            XElement variableElement = new XElement(xNS + "li",
                    new XAttribute("class", "forte-variable-wrapper" + xJSTreeClass),
                    new XAttribute("id", forteVariable.ID),
                    dataAttr,
                    new XElement(xNS + "a", new XAttribute("class", "forte-variable-display-name")) { Value = forteVariable.DisplayName });

            //render to the DOM
            XElement variableValueElement = GetVariableValueElement(forteVariable, true, xNS);
            variableElement.Add(variableValueElement);

            return variableElement;
        }
        private XElement[] GetJsTreeHeaderElements(XNamespace xNS)
        {
            return new XElement[] {
                   new XElement(xNS + "link", new XAttribute("rel", "stylesheet"), new XAttribute("href", "https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css")),
                   new XElement(xNS + "script", new XAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js")) { Value = string.Empty },
                   new XElement(xNS + "script", new XAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js")) { Value = string.Empty }};
        }
        private XElement[] GetFancyTreeHeaderElements(XNamespace xNS)
        {
            return new XElement[] {
                   new XElement(xNS + "link", new XAttribute("rel", "stylesheet"), new XAttribute("href", "https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.22.1/skin-win8/ui.fancytree.min.css")),
                   new XElement(xNS + "script", new XAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js")) { Value = string.Empty },
                   new XElement(xNS + "script", new XAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js")) { Value = string.Empty },
                   new XElement(xNS + "script", new XAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.22.1/jquery.fancytree.js")) { Value = string.Empty}};
        }
        private XElement[] GetHtmlHeaderElements(XNamespace xNS)
        {
            return new XElement[] {
                   new XElement(xNS + "script", new XAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js")) { Value = string.Empty }};
        }
        private XElement GetHeaderStylesElement(TreeControlTypes iFormat, XNamespace xNS)
        {
            switch (iFormat)
            {
                case TreeControlTypes.JSTree:
                    return new XElement(xNS + "link", new XAttribute("rel", "stylesheet"), new XAttribute("href", "Forte-jstree.css"));
                case TreeControlTypes.FancyTree:
                    return new XElement(xNS + "link", new XAttribute("rel", "stylesheet"), new XAttribute("href", "Forte-ftree.css"));
                default:
                    return new XElement(xNS + "link", new XAttribute("rel", "stylesheet"), new XAttribute("href", "Forte.css"));
            }
        }
        private XElement GetVariableValueElement(XmlVariable forteVariable, bool bShowValue, XNamespace namespaceAttr)
        {
            //get control properties for variable
            string xProps = forteVariable.ControlProperties;
            string[] aProps = forteVariable.ControlProperties.Split(LMP.StringArray.mpEndOfSubValue);

            //if (bShowValue)
            //{
            string xInputID = "input_" + forteVariable.ID;
            string xListID = "list_" + forteVariable.ID;

            //massage value
            string xValue = forteVariable.Value;
            string xDisplayValue = GetVariableUIValue(forteVariable);
            xValue = xValue.Replace('\v', '\r');
            //get element for control
            XElement forteCtl = null;
            XElement ctlData = null;
            XElement forteJs = null;

            //get control html based on control type
            switch (forteVariable.ControlType)
            {
                case LMP.Data.mpControlTypes.DropdownList:
                    TSG.WebControls.DropdownList dropdownlist = new TSG.WebControls.DropdownList(forteVariable, namespaceAttr);
                    forteCtl = dropdownlist.XElement;
                    break;

                case LMP.Data.mpControlTypes.Combo:
                    TSG.WebControls.Combo combo = new TSG.WebControls.Combo(forteVariable, namespaceAttr);
                    forteCtl = combo.XElement;
                    break;

                case LMP.Data.mpControlTypes.MultilineCombo:
                    TSG.WebControls.MultilineCombo multilineCombo = new TSG.WebControls.MultilineCombo(forteVariable, namespaceAttr);
                    forteCtl = multilineCombo.XElement;
                    break;

                case LMP.Data.mpControlTypes.MultilineTextbox:
                    TSG.WebControls.MultilineTextbox multilineTextbox = new TSG.WebControls.MultilineTextbox(forteVariable, namespaceAttr);
                    forteCtl = multilineTextbox.XElement;
                    break;

                case LMP.Data.mpControlTypes.DetailGrid:
                    TSG.WebControls.DetailGrid detailGrid = new WebControls.DetailGrid(forteVariable, namespaceAttr);
                    forteCtl = detailGrid.XElement;
                    break;

                case LMP.Data.mpControlTypes.RelineGrid:
                case LMP.Data.mpControlTypes.DetailList:
                    //changed format from Jeffrey's original - dcf - 05/12/2017
                    forteCtl = new XElement(namespaceAttr + "textarea", new XAttribute("id", xInputID), new XAttribute("rows", "3"), new XAttribute("cols", "60"),
                        new XAttribute("class", "forte-control"), new XAttribute("width", "50%"), new XAttribute("style", "max-width:100%"))
                    { Value = xValue };
                    break;
                case LMP.Data.mpControlTypes.Checkbox:
                    forteCtl = new XElement(namespaceAttr + "select", new XAttribute("id", xInputID), new XAttribute("class", "checkbox forte-control"));
                    string xTrueText = "Yes";
                    string xFalseText = "No";
                    foreach (string xProp in aProps)
                    {
                        if (xProp.ToLower().StartsWith("truestring"))
                        {
                            string[] aVal = xProp.Split('=');
                            xTrueText = XmlExpression.Evaluate(aVal[1], forteVariable.Segment, forteVariable.Segment.ForteDocument);
                        }
                        else if (xProp.ToLower().StartsWith("falsestring"))
                        {
                            string[] aVal = xProp.Split('=');
                            xFalseText = XmlExpression.Evaluate(aVal[1], forteVariable.Segment, forteVariable.Segment.ForteDocument);
                        }
                    }

                    forteCtl.Add(new XElement(namespaceAttr + "option", new XAttribute("value", "true"),
                        forteVariable.Value.ToLower() == "true" ? new XAttribute("selected", "true") : null)
                    { Value = xTrueText });
                    forteCtl.Add(new XElement(namespaceAttr + "option", new XAttribute("value", "false"),
                        forteVariable.Value.ToLower() != "true" ? new XAttribute("selected", "true") : null)
                    { Value = xFalseText });
                    break;

                case LMP.Data.mpControlTypes.DateCombo:
                    TSG.WebControls.DateCombo oDateCombo = new TSG.WebControls.DateCombo(forteVariable, namespaceAttr);
                    forteCtl = oDateCombo.XElement;
                    break;
                default:
                    forteCtl = new XElement(namespaceAttr + "input", new XAttribute("id", xInputID), new XAttribute("type", "text"), new XAttribute("value", xValue), new XAttribute("class", "forte-control"));
                    break;
            }

            //xControl = new XElement(xNS + "p", new XAttribute("class", "control-para"), xControl);
            if (ctlData != null)
            {
                ctlData = new XElement(namespaceAttr + "p", new XAttribute("class", "list-para"), ctlData);
            }

            XElement xInput = new XElement(namespaceAttr + "ul", new XAttribute("class", "forte-variable-value"),
                new XElement(namespaceAttr + "li", new XAttribute("class", "forte-variable-display-value"), new XAttribute("data-icon", ""), new XElement(namespaceAttr + "a") { Value = xDisplayValue }),
                new XElement(namespaceAttr + "li", new XAttribute("class", "forte-variable-control-wrapper"), new XAttribute("data-icon", ""), new XAttribute("style", "display: none;"),
                    forteCtl, ctlData, forteJs));

            return xInput;
            //}
        }
        /// <summary>
        /// returns the value to be displayed 
        /// in the tree for the specified variable
        /// </summary>
        /// <param name="forteVariable"></param>
        /// <returns></returns>
        private string GetVariableUIValue(LMP.Architect.Oxml.XmlVariable forteVariable)
        {
            string xEmptyDisplayValue = GetEmptyValueDisplay();
            string xValue = null;

            //check if variable.DisplayValue contains a field code, date format
            //or boolean value, then process accordingly

            if (System.String.IsNullOrEmpty(forteVariable.DisplayValue))
            {
                xValue = forteVariable.Value;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                xValue = LMP.Architect.Oxml.XmlExpression.MarkLiteralReservedCharacters(xValue);
                xValue = LMP.Architect.Oxml.XmlExpression.Evaluate(xValue, forteVariable.Segment, forteVariable.Segment.ForteDocument);

                if (forteVariable.ControlType == LMP.Data.mpControlTypes.Checkbox)
                {
                    if (!forteVariable.IsMultiValue)
                    {
                        //Display appropriate True/False text based on control properties-
                        BooleanComboBox oBC = forteVariable.AssociatedControl as BooleanComboBox;
                        if (oBC == null)
                        {
                            oBC = (BooleanComboBox)forteVariable.CreateAssociatedControl(null);
                        }
                        if (xValue.ToUpper() == "TRUE")
                            xValue = oBC.TrueString;
                        else if (xValue.ToUpper() == "FALSE")
                            xValue = oBC.FalseString;
                    }
                    else
                    {
                        //AssociatedControl is a MultiValueControl configured in BooleanComboBox mode
                        //Display appropriate True/False text based on control properties-
                        MultiValueControl oMVC = (MultiValueControl)forteVariable.AssociatedControl;
                        if (oMVC == null)
                        {
                            oMVC = (MultiValueControl)forteVariable.CreateAssociatedControl(null);

                        }
                        xValue = xValue.Replace("true", oMVC.TrueString);
                        xValue = xValue.Replace("false", oMVC.FalseString);
                    }
                }
                if (forteVariable.IsMultiValue)
                {
                    //Replace delimiter between multiple values;
                    xValue = xValue.Replace(LMP.StringArray.mpEndOfValue.ToString(), "; ");
                }
            }
            else
            {
                //evaluate the display value expression using variable value
                try
                {
                    xValue = EvaluateDisplayValue_Oxml(forteVariable);

                    //evaluate the display value expression using variable value
                    try
                    {
                        //GLOG 3318: reflect use of field code in display value
                        if (forteVariable.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT") &&
                            forteVariable.AssociatedContentControls[0].Descendants<FieldChar>().Any())
                            xValue = xValue + " (field code)";
                    }
                    catch { }
                }
                catch { }
            }

            if (xValue == null || xValue == "")
                xValue = xEmptyDisplayValue;
            else
            {
                //replace non-printing chars with ellipses
                xValue = xValue.Replace("\v", "...");
                xValue = xValue.Replace("\t", "...");
                xValue = xValue.Replace("\r\n", "...");
                xValue = xValue.Replace("\r", "...");
                xValue = xValue.Replace("\n", "...");
                xValue = (forteVariable.Value == "") ? xEmptyDisplayValue : xValue;
            }

            //get available value node text width
            //TODO: we may want to enhance this method
            //to determine how much text to enter into
            //the node, given how much width is available
            if (xValue.Length > 35)
                //trim string and append ellipse if necessary
                xValue = xValue.Substring(0, 35) +
                    (xValue.EndsWith("...") ? "" : "...");

            return xValue;
        }
        private string GetEmptyValueDisplay()
        {
            return LMP.Resources.GetLangString("Msg_EmptyValueDisplayValue");
        }
        private string EvaluateDisplayValue_Oxml(LMP.Architect.Oxml.XmlVariable oVar)
        {
            DateTime t0 = DateTime.Now;

            if (oVar.DisplayValue == null || oVar.DisplayValue == "")
                return "";

            string xVarValue = oVar.Value;
            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = XmlExpression.MarkLiteralReservedCharacters(xVarValue);
            //pre-evaluate the DisplayValue field code(s), substituting MyValue for oVar.Value
            //string xFieldCode = oVar.DisplayValue.Replace("[MyValue]", xVarValue);
            string xFieldCode = XmlFieldCode.EvaluateMyValue(oVar.DisplayValue, xVarValue);

            if (xFieldCode.Contains("[MyTagValue]"))
            {
                try
                {
                    SdtElement oCC = oVar.AssociatedContentControls[0];
                    string xTagValue = oCC.InnerText;
                    //If CC contains a FieldCode, get the field result from the Text element
                    if (oCC.Descendants<FieldChar>().Any())
                    {
                        DocumentFormat.OpenXml.OpenXmlElement oText = oCC.Descendants<Text>().FirstOrDefault();
                        if (oText != null)
                        {
                            xTagValue = oText.InnerText;
                        }
                    }

                    xTagValue = XmlExpression.MarkLiteralReservedCharacters(xTagValue);

                    xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                }
                catch
                {
                    xFieldCode = xFieldCode.Replace("[MyTagValue]", "");
                }
            }

            //xFieldCode = xFieldCode.Replace("MyValue", xVarValue);
            xFieldCode = XmlFieldCode.EvaluateMyValue(xFieldCode, xVarValue);

            //handle listlookup field code
            if (oVar.DisplayValue.StartsWith("[ListLookup"))
                return LMP.Architect.Oxml.XmlExpression.Evaluate(xFieldCode, oVar.Segment, oVar.Segment.ForteDocument);

            //check for ^AND operator in compound DisplayValue strings 
            //or "][" indicating multiple field codes to be evaluated -
            //reline control value, which contains nodes for Standard and Special
            //is an example where DisplayValue may contain two field codes

            //***REPLACING THESE CHARACTERS DOESN'T SEEM TO BE NECESSARY, AND DOES NOT WORK CORRECTLY WITH
            //***MORE COMPLEX EXPRESSIONS - LET EXPRESSION.EVALUATE DEAL WITH THIS IN THE NORMAL FASHION
            //xFieldCode = xFieldCode.Replace("^AND", LMP.StringArray.mpEndOfElement.ToString() + "[");
            //xFieldCode = xFieldCode.Replace("][", "]" + LMP.StringArray.mpEndOfElement.ToString() + "[");

            //create array of field codes for further pre-evaluation
            string[] xFieldCodes = xFieldCode.Split(LMP.StringArray.mpEndOfElement);
            StringBuilder oSB = new StringBuilder();

            //build an array list of values returned by Expression.Evaluate
            ArrayList oArrayList = new ArrayList();
            string xTemp = null;
            string[] aTemp = null;

            //for each configured field code, evaluate and return its string value
            //in the case of CI field codes, the string will be delimited by "; "
            //one member for each recipient
            for (int i = 0; i <= xFieldCodes.GetUpperBound(0); i++)
            {
                xTemp = xFieldCodes[i];
                xTemp = xTemp.Replace("; ", ";mp10Tempmp10 ");
                if (xTemp == "[TagValue]" || xTemp == "[MyTagValue]")
                {
                    xTemp = oVar.AssociatedContentControls[0].InnerText;
                }
                else
                {
                    xTemp = LMP.Architect.Oxml.XmlExpression.Evaluate(
                        xTemp, oVar.Segment, oVar.Segment.ForteDocument);
                }

                if (xTemp == null)
                    xTemp = "";

                //split value and add to arraylist
                xTemp = xTemp.Replace(";mp10Tempmp10 ", ";");
                xTemp = xTemp.Replace("mp10Tempmp10 ", " ");
                xTemp = xTemp.Replace("; ", "|");
                aTemp = xTemp.Split('|');
                oArrayList.Add(aTemp);
            }

            //go through the array list of field codes and match field code return values
            //In most cases aTemp will have only a single member.  For CI type field codes,
            //aTemp will have as many members as returned contacts.
            for (int i = 0; i < oArrayList.Count; i++)
            {
                string[] aItems = (string[])oArrayList[i];

                for (int j = 0; j < aItems.Length; j++)
                {
                    //delimit each record w/ semicolon, add
                    //commas between record fields, i.e, name, address; name2, address;
                    string xFormat = (j == aItems.Length - 1) ? "{0}; " : "{0}, ";

                    oSB.AppendFormat(xFormat, aItems[j]);
                }
            }

            string xValue = oSB.ToString();
            if (oVar.IsMultiValue)
            {
                //Replace value separator for display
                xValue = xValue.Replace(LMP.StringArray.mpEndOfValue.ToString(), "; ");
            }
            //eliminate double delimiters caused by empty values
            xValue = xValue.Replace(";;", ";");
            xValue = xValue.Replace("; ;", ";");
            xValue = xValue.Replace(",,", ",");
            xValue = xValue.Replace(", ,", ",");

            xValue = xValue.TrimEnd();
            xValue = xValue.TrimEnd(';');
            xValue = xValue.TrimStart(';');
            xValue = xValue.TrimStart(',');
            xValue = xValue.TrimStart();

            LMP.Benchmarks.Print(t0, oVar.Name);

            return xValue;
        }
        #endregion
    }
}
