﻿var lastnode;
var editmode = false;

function OnLoadHandler() {
	$('#forte-segment-tree').fancytree( { 
		minExpandLevel: 2, 
		keyboard: true, 
		tabindex:0,
		selectMode:3,
		focusonSelect:true,
		activate: OnVariableNodeSelect,
		beforeExpand: BeforeNodeExpanded,
		expand: OnNodeExpanded
	 });
	 $('#forte-segment-tree').fancytree("getTree").visit(function(node){
	  if (node.extraClasses.includes("forte-variable-control-wrapper")) 
	  {
	  	$(node.li).hide();
	  }
	  else if (node.extraClasses.includes("forte-child-segment"))
	  {
	  	node.load(true).done(function() {
	  		node.visit(function(childNode){
				  if (childNode.extraClasses.includes("forte-variable-control-wrapper")) 
				  {
				  	alert(childNode);
				  	$(childNode.li).hide();
				  }
	  			
	  		})
	  	})
	  }
  });      
//	$('#forte-segment-tree').on("close_node.fancystree", PreventVariableCloseNode);
//	$('#forte-segment-tree').on("open_node.fancystree", OnVariableNodeOpen);
//	$('#forte-segment-tree').on("select_node.fancytree", OnVariableNodeSelect);
//	$('#forte-segment-tree').on('click', ':input', EnterEditMode);
	
//	var v =$("#forte-segment-tree").fancytree(true).get_json('#', {'flat': true});
//	for (i = 0; i < v.length; i++) {
//	    var z = v[i];
//	    VariableNodeInitialize(z.id);
//	}
}
function OnNodeExpanded(evt, data) {
	if (data.node.extraClasses.includes('forte-child-segment')) 
	{
		var curNode = data.node;
		if (curNode.isExpanded() == true)
		{
	                curNode.visit(function(childNode) {
				if (childNode.extraClasses.includes("forte-variable-control-wrapper"))
				{
				  	$(childNode.li).hide();
			  	}
			})
		}
	}
}
function BeforeNodeExpanded(evt, data) {
	if (data.node.extraClasses.includes("forte-variable-wrapper"))
	{
		//Prevent collapsing Variable nodes
		if (data.node.isExpanded())
		{
			return false;
		}
	}
	return true;
}
function OnVariableNodeSelect(evt, data) {
	var node = [];
	if (data.node.extraClasses.includes("forte-variable-wrapper"))
	{
		node = data.node;
	}
	else if (data.node.extraClasses.includes("forte-variable-display-value"))
	{
		node = data.node.parent;
	}
	else
	{
		return true;
	}
	if (node != lastnode)
	{
		var node1 = node.children[0];
		var node2 = node.children[1];
		$(node1.li).hide();
		$(node2.li).show();
		if (lastnode != undefined) {
			node1 = lastnode.children[0];
			node2 = lastnode.children[1];
			$(node2.li).hide();
			$(node1.li).show();
		}
		lastnode = node;
	}
}
