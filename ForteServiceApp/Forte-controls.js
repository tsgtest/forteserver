﻿function GetControlVariableName(control) {
    var assocVarName = control.getAttribute("data-forte-variable");
    return assocVarName === undefined ? "key" : assocVarName;
}

function GetControlValue(control) {
    var controlType = control.getAttribute("data-forte-control-type");
    var idBase = control.id.substring(3);

    switch (controlType) {
        case "MultilineCombo":
            return control.children["txtMain" + idBase].value;
        case "Combo":
            return control.children["cmbMain" + idBase].value;
        default:
            return control.value;
    }
}

function GetPrefill() {
    var forteCtls = document.getElementsByClassName("forte-control");
    var dataString = "<Prefill>";
    for (var ctl of forteCtls) {
        var value = GetControlValue(ctl);
        var name = GetControlVariableName(ctl);
        dataString += "<" + name + ">" + GetControlValue(ctl) + "</" + name + ">";
    }

    return dataString + "</Prefill>";
}

function ShowPrefill() {
    var prefill = GetPrefill();
    alert(prefill);
}

function MlcAddSelection(control) {
    var newText = "";
    var newline = String.fromCharCode(13, 10);
    var idBase = control.id.substring(3);
    var selVal = control.children["lstMain" + idBase].value;
    var mainTextbox = control.children["txtMain" + idBase];
    var existingText = "";
    existingText = mainTextbox.value;

    if (existingText !== "") {
        newText = existingText + newline + selVal;
    }
    else {
        newText = selVal;
    }

    mainTextbox.value = newText;
}

function MlcCloseDropdown(control) {
    var idBase = control.id.substring(3);
    var dd = control.children["lstMain" + idBase];
    dd.style.display = "none";
}

function MlcToggleDropdown(control) {
    var idBase = control.id.substring(3);
    var dd = control.children["lstMain" + idBase];
    var isVisible = dd.style.display !== "none";
    dd.style.display = isVisible ? "none" : "inline";
}

function DateComboSelection(control) {
    var idBase = control.id.substring(2);
    var selIndex = control.children["lstMain" + idBase].selectedIndex;
    var selText = control.children["lstMain" + idBase].options[selIndex].text;
    var mainTextbox = control.children["txtMain" + idBase];
    mainTextbox.value = selText;

    var selVal = control.children["lstMain" + idBase].value;
    if (selVal.includes("/F")){
	mainTextbox.readOnly = true;
    }
    else {
	mainTextbox.removeAttribute("readonly");
    }     
}

function ComboSelection(control) {
    var idBase = control.id.substring(2);
    var selIndex = control.children["lstMain" + idBase].selectedIndex;
    var selText = control.children["lstMain" + idBase].options[selIndex].text;
    var mainTextbox = control.children["txtMain" + idBase];
    mainTextbox.value = selText;
}

function ComboToggleDropdown(control) {
    var idBase = control.id.substring(2);
    var dd = control.children["lstMain" + idBase];
    var isVisible = dd.style.display !== "none";
    dd.style.display = isVisible ? "none" : "inline";
}

function ComboCloseDropdown(control) {
    var idBase = control.id.substring(2);
    var dd = control.children["lstMain" + idBase];
    dd.style.display = "none";
}

function DetailGridMovePrev(control) {
    var idBase = control.id.substring(3);
    var iCurrent = control.getAttribute("data-detail-index");
    if (iCurrent < 1)
        return;

    var detailsList = JSON.parse(control.getAttribute("data-detail-list"));
    var iDetailCount = detailsList.length;
    iCurrent--;
    var itemList = detailsList[iCurrent];
    for (i = 1; i < itemList.length; i++) {
        var dtlId = "detail" + i + "_" + idBase;
        var dtl = control.children[dtlId];
        if (dtl.nodeName == "INPUT") {
            dtl.setAttribute("value", itemList[i]);
        }
        else {
            dtl.value = itemList[i];
        }
    }
    control.setAttribute("data-detail-index", iCurrent);
    var lbl = control.children["lblItem" + idBase];
    var counterScheme = control.getAttribute("data-detail-counterscheme");
    if (counterScheme == "") {
        counterScheme = "Item %0 of %1";
    }
    iCurrent++;
    lbl.innerText = counterScheme.replace("%0", iCurrent).replace("%1", iDetailCount);
}
function DetailGridMoveNext(control) {
    var idBase = control.id.substring(3);
    var iCurrent = control.getAttribute("data-detail-index");
    var detailsList = JSON.parse(control.getAttribute("data-detail-list"));
    if (detailsList.length <= (iCurrent + 1)) {
        return;
    }
    var iDetailCount = detailsList.length;
    iCurrent++;
    var itemList = detailsList[iCurrent];
    for (i = 1; i < itemList.length; i++) {
        var dtlId = "detail" + i + "_" + idBase;
        var dtl = control.children[dtlId];
        if (dtl.nodeName == "INPUT") {
            dtl.setAttribute("value", itemList[i]);
        }
        else {
            dtl.value = itemList[i];
        }
    }
    control.setAttribute("data-detail-index", iCurrent);
    var lbl = control.children["lblItem" + idBase];
    var counterScheme = control.getAttribute("data-detail-counterscheme");
    if (counterScheme == "") {
        counterScheme = "Item %0 of %1";
    }
    iCurrent++;
    lbl.innerText = counterScheme.replace("%0", iCurrent).replace("%1", iDetailCount);
}