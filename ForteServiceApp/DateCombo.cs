﻿using System;
using System.Text;
using LMP.Architect.Oxml;
using System.Xml;
using System.Xml.Linq;
namespace TSG.WebControls
{
    public class DateCombo : ForteWebComboBase
    {
        public enum DateComboModes
        {
            Literal = 0,
            Format = 1
        }

        private const char ITEM_SEP = LMP.StringArray.mpEndOfSubField;
        private int m_iLCID = 1033;

        public DateCombo(XmlVariable segmentVariable, XNamespace namespaceAttr) : base(segmentVariable, namespaceAttr)
        {
            if (this.ControlProperties["Formats"] != null)
                this.Formats = this.ControlProperties["Formats"].ToString();
            if (this.ControlProperties["ListName"] != null)
                this.ListName = this.ControlProperties["ListName"].ToString();
            if (this.ControlProperties["ListRows"] != null)
                this.ListRows = int.Parse(this.ControlProperties["ListRows"].ToString());
            if (this.ControlProperties["MaxDropDownItems"] != null)
                this.MaxDropDownItems = int.Parse(this.ControlProperties["MaxDropDownItems"].ToString());
            if ((this.ControlProperties["Mode"] != null) && (this.ControlProperties["Mode"].ToString() == "Literal"))
                this.Mode = DateComboModes.Literal;
            else
                this.Mode = DateComboModes.Format;
            if (this.ControlProperties["Height"] != null)
                this.Height = this.ControlProperties["Height"].ToString();
            if (this.ControlProperties["LCID"] != null)
                this.LCID = int.Parse(this.ControlProperties["LCID"].ToString());
        }

        public string Formats { get; set; }
        public int MaxDropDownItems { get; set; }
        public DateComboModes Mode { get; set; }

        public int LCID
        {
            get { return m_iLCID; }
            set
            {
                m_iLCID = value;
            }
        }

        private string GetDateListHtml()
        {
            string xListItems = this.Formats.Replace("&quot;", "\"");
            string[] aList = xListItems.Split(ITEM_SEP);
            string[,] aDateFormats = new string[aList.GetUpperBound(0) + 1, 2];
            DateTime oDT = DateTime.Now;
            for (int i = 0; i <= aList.GetUpperBound(0); i++)
            {
                if (this.Mode == DateComboModes.Format)
                    aDateFormats[i, 0] = GetFormattedDate(oDT, aList[i]);
                else
                    aDateFormats[i, 0] = aList[i];
                aDateFormats[i, 1] = aList[i];
            }

            StringBuilder sb = new StringBuilder();

            for (int l = 0; l < aList.Length; l++)
            {
                XElement newOption = new XElement(this.Namespace + "option",
                    new XAttribute("value", aDateFormats[l, 1]),
                    aDateFormats[l, 1] == this.Value ? new XAttribute("selected", "selected") : null)
                    { Value = aDateFormats[l, 0] };
                sb.AppendLine(newOption.ToString());
            }
            return sb.ToString();
        }

        /// <summary>
        /// converts date format strings to display text
        /// handles jurat-type dates
        /// </summary>
        /// <param name="oDateTime"></param>
        /// <param name="xDateFormat"></param>
        /// <returns></returns>
        private string GetFormattedDate(DateTime oDateTime, string xDateFormat)
        {
            return LMP.String.EvaluateDateTimeValue(xDateFormat, oDateTime, this.LCID);
        }

        #region ForteWebControl overrides
        public override string ToHtml()
        {
            StringBuilder oSB = new StringBuilder();
            string xListItemHtml = "";

            if (!string.IsNullOrEmpty(this.ListName))
                xListItemHtml = GetListItemHtml(this.ListName);
            else
                xListItemHtml = GetDateListHtml();

            string xValue = "";
            if (this.Mode == DateComboModes.Format)
            {
                DateTime oDT = DateTime.Now;
                xValue = GetFormattedDate(oDT, this.Value);
            }
            else
                xValue = this.Value;

            oSB.AppendFormat("<div id='dc{0}' class='forte-control' data-forte-control-type='DateCombo' data-forte-variable='{1}' data-forte-segment='{2}' style='position: relative; left: {3}; height: {4}; z - index: 1;'>" +
                "<input name = 'MainTextbox' id='txtMain{0}' type='text' value='{5}' class='forte-control-mlc-textarea' />" +
                "<input type='button' name='DropDownButton' value='v' id='btnDD{0}' onclick='ComboToggleDropdown(this.parentElement)' class='forte-control-mlc-button' />" +
                "<select name='MainListbox' id='lstMain{0}' size='{6}' onclick='DateComboSelection(this.parentElement)' onblur='ComboCloseDropdown(this.parentElement)' class='forte-control-mlc-select' xmlns='{8}' > {7} </select > </div >",
                Guid.NewGuid().ToString(), this.AssociatedVariableName, this.SegmentName, this.Left == "" ? "0px" : this.Left, this.Height, xValue, this.ListRows, xListItemHtml, this.Namespace);
            return oSB.ToString();
        }
        #endregion
    }
}
