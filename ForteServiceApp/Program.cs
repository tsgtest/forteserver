﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace ForteServiceApp
{
    static class Program
    {
        public static ForteServiceForm forteServiceForm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            forteServiceForm = new ForteServiceForm();
            Application.Run(forteServiceForm);
        }
    }
}
