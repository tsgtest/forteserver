﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMP.Architect.Oxml;
using System.Xml;
using System.Xml.Linq;
using TSG.WebControls;
using LMP.Controls;
using DocumentFormat.OpenXml.Wordprocessing;
using LMP.Grail;

namespace TSG.Forte
{
    public class VariableValueNode
    {
        XmlVariable variable;

        public VariableValueNode(XmlVariable var)
        {
            variable = var;
        }

        public XElement ToXElement(bool bShowValue, XNamespace namespaceAttr)
        {
            //get control properties for variable
            string xProps = this.variable.ControlProperties;
            string[] aProps = this.variable.ControlProperties.Split(LMP.StringArray.mpEndOfSubValue);

            //if (bShowValue)
            //{
                string xInputID = "input_" + this.variable.ID;
                string xListID = "list_" + this.variable.ID;

                //massage value
                string xValue = this.variable.Value;
                string xDisplayValue = GetVariableUIValue(this.variable);
                xValue = xValue.Replace('\v', '\r');

                //get element for control
                XElement forteCtl = null;
                XElement ctlData = null;
                XElement forteJs = null;

                //get control html based on control type
                switch (this.variable.ControlType)
                {
                    case LMP.Data.mpControlTypes.DropdownList:
                    case LMP.Data.mpControlTypes.Combo:
                        TSG.WebControls.Combo combo = new TSG.WebControls.Combo(this.variable);
                        forteCtl = combo.XElement;
                        break;

                    case LMP.Data.mpControlTypes.MultilineCombo:
                        TSG.WebControls.MultilineCombo multilineCombo = new TSG.WebControls.MultilineCombo(this.variable);
                        forteCtl = multilineCombo.XElement;
                        break;

                    case LMP.Data.mpControlTypes.MultilineTextbox:
                    case LMP.Data.mpControlTypes.DetailGrid:
                    case LMP.Data.mpControlTypes.RelineGrid:
                    case LMP.Data.mpControlTypes.DetailList:
                        //changed format from Jeffrey's original - dcf - 05/12/2017
                        forteCtl = new XElement(namespaceAttr + "textarea", new XAttribute("id", xInputID), new XAttribute("rows", "3"), new XAttribute("cols", "60"),
                            new XAttribute("class", "forte-control"), new XAttribute("width", "50%"), new XAttribute("style", "max-width:100%"))
                        { Value = xValue };
                        break;
                    case LMP.Data.mpControlTypes.Checkbox:
                        TSG.WebControls.CheckBox checkBox = new TSG.WebControls.CheckBox(this.variable);
                        forteCtl = checkBox.XElement;
                        break;
                case LMP.Data.mpControlTypes.DateCombo:
                        forteCtl = new XElement(namespaceAttr + "select", new XAttribute("id", xInputID), new XAttribute("class", "forte-control"));
                        foreach (string xProp in aProps)
                        {
                            if (xProp.ToLower().StartsWith("formats"))
                            {
                                string[] aVal = xProp.Split('=');
                                string xFormats = XmlExpression.Evaluate(aVal[1], this.variable.Segment, this.variable.Segment.ForteDocument);
                                string[] aFormats = xFormats.Split(LMP.StringArray.mpEndOfSubField);
                                foreach (string xFormat in aFormats)
                                {
                                    forteCtl.Add(new XElement(namespaceAttr + "option", new XAttribute("value", xFormat),
                                        this.variable.Value == xFormat ? new XAttribute("selected", "true") : null)
                                    { Value = xFormat });
                                }
                                break;
                            }
                        }
                        break;
                    default:
                        forteCtl = new XElement(namespaceAttr + "input", new XAttribute("id", xInputID), new XAttribute("type", "text"), new XAttribute("value", xValue), new XAttribute("class", "forte-control"));
                        break;
                }

                //xControl = new XElement(xNS + "p", new XAttribute("class", "control-para"), xControl);
                if (ctlData != null)
                {
                    ctlData = new XElement(namespaceAttr + "p", new XAttribute("class", "list-para"), ctlData);
                }

                XElement xInput = new XElement(namespaceAttr + "ul", new XAttribute("class", "forte-variable-value"),
                    new XElement(namespaceAttr + "li", new XAttribute("class", "forte-variable-display-value"), new XAttribute("data-icon", ""), new XElement(namespaceAttr + "a") { Value = xDisplayValue }),
                    new XElement(namespaceAttr + "li", new XAttribute("class", "forte-variable-control-wrapper"), new XAttribute("data-icon", ""),
                        forteCtl, ctlData, forteJs));

            return xInput;
            //}
        }

        /// <summary>
        /// returns the value to be displayed 
        /// in the tree for the specified variable
        /// </summary>
        /// <param name="oVar"></param>
        /// <returns></returns>
        private string GetVariableUIValue(LMP.Architect.Oxml.XmlVariable oVar)
        {
            string xEmptyDisplayValue = GetEmptyValueDisplay();
            string xValue = null;

            //check if variable.DisplayValue contains a field code, date format
            //or boolean value, then process accordingly

            if (System.String.IsNullOrEmpty(oVar.DisplayValue))
            {
                xValue = oVar.Value;
                //GLOG 2151: Replace any reserved Expression characters before evaluating
                xValue = LMP.Architect.Oxml.XmlExpression.MarkLiteralReservedCharacters(xValue);
                xValue = LMP.Architect.Oxml.XmlExpression.Evaluate(xValue, oVar.Segment, oVar.Segment.ForteDocument);

                if (oVar.ControlType == LMP.Data.mpControlTypes.Checkbox)
                {
                    if (!oVar.IsMultiValue)
                    {
                        //Display appropriate True/False text based on control properties-
                        BooleanComboBox oBC = oVar.AssociatedControl as BooleanComboBox;
                        if (oBC == null)
                        {
                            oBC = (BooleanComboBox)oVar.CreateAssociatedControl(null);
                        }
                        if (xValue.ToUpper() == "TRUE")
                            xValue = oBC.TrueString;
                        else if (xValue.ToUpper() == "FALSE")
                            xValue = oBC.FalseString;
                    }
                    else
                    {
                        //AssociatedControl is a MultiValueControl configured in BooleanComboBox mode
                        //Display appropriate True/False text based on control properties-
                        MultiValueControl oMVC = (MultiValueControl)oVar.AssociatedControl;
                        if (oMVC == null)
                        {
                            oMVC = (MultiValueControl)oVar.CreateAssociatedControl(null);

                        }
                        xValue = xValue.Replace("true", oMVC.TrueString);
                        xValue = xValue.Replace("false", oMVC.FalseString);
                    }
                }
                if (oVar.IsMultiValue)
                {
                    //Replace delimiter between multiple values;
                    xValue = xValue.Replace(LMP.StringArray.mpEndOfValue.ToString(), "; ");
                }
            }
            else
            {
                //evaluate the display value expression using variable value
                try
                {
                    xValue = EvaluateDisplayValue_Oxml(oVar);

                    //evaluate the display value expression using variable value
                    try
                    {
                        //GLOG 3318: reflect use of field code in display value
                        if (oVar.ValueSourceExpression.ToUpper().Contains("[DATEFORMAT") &&
                            oVar.AssociatedContentControls[0].Descendants<FieldChar>().Any())
                            xValue = xValue + " (field code)";
                    }
                    catch { }
                }
                catch { }
            }

            if (xValue == null || xValue == "")
                xValue = xEmptyDisplayValue;
            else
            {
                //replace non-printing chars with ellipses
                xValue = xValue.Replace("\v", "...");
                xValue = xValue.Replace("\t", "...");
                xValue = xValue.Replace("\r\n", "...");
                xValue = xValue.Replace("\r", "...");
                xValue = xValue.Replace("\n", "...");
                xValue = (oVar.Value == "") ? xEmptyDisplayValue : xValue;
            }

            //get available value node text width
            //TODO: we may want to enhance this method
            //to determine how much text to enter into
            //the node, given how much width is available
            if (xValue.Length > 35)
                //trim string and append ellipse if necessary
                xValue = xValue.Substring(0, 35) +
                    (xValue.EndsWith("...") ? "" : "...");

            return xValue;
        }
        private string GetEmptyValueDisplay()
        {
            return LMP.Resources.GetLangString("Msg_EmptyValueDisplayValue");
        }
        private string EvaluateDisplayValue_Oxml(LMP.Architect.Oxml.XmlVariable oVar)
        {
            DateTime t0 = DateTime.Now;

            if (oVar.DisplayValue == null || oVar.DisplayValue == "")
                return "";

            string xVarValue = oVar.Value;
            //GLOG 2151: Mark reserved expression characters in Value so they'll
            //be treated as the literal characters in Expression.Evaluate
            xVarValue = XmlExpression.MarkLiteralReservedCharacters(xVarValue);
            //pre-evaluate the DisplayValue field code(s), substituting MyValue for oVar.Value
            //string xFieldCode = oVar.DisplayValue.Replace("[MyValue]", xVarValue);
            string xFieldCode = XmlFieldCode.EvaluateMyValue(oVar.DisplayValue, xVarValue);

            if (xFieldCode.Contains("[MyTagValue]"))
            {
                try
                {
                    SdtElement oCC = oVar.AssociatedContentControls[0];
                    string xTagValue = oCC.InnerText;
                    //If CC contains a FieldCode, get the field result from the Text element
                    if (oCC.Descendants<FieldChar>().Any())
                    {
                        DocumentFormat.OpenXml.OpenXmlElement oText = oCC.Descendants<Text>().FirstOrDefault();
                        if (oText != null)
                        {
                            xTagValue = oText.InnerText;
                        }
                    }

                    xTagValue = XmlExpression.MarkLiteralReservedCharacters(xTagValue);

                    xFieldCode = xFieldCode.Replace("[MyTagValue]", xTagValue);
                }
                catch
                {
                    xFieldCode = xFieldCode.Replace("[MyTagValue]", "");
                }
            }

            //xFieldCode = xFieldCode.Replace("MyValue", xVarValue);
            xFieldCode = XmlFieldCode.EvaluateMyValue(xFieldCode, xVarValue);

            //handle listlookup field code
            if (oVar.DisplayValue.StartsWith("[ListLookup"))
                return LMP.Architect.Oxml.XmlExpression.Evaluate(xFieldCode, oVar.Segment, oVar.Segment.ForteDocument);

            //check for ^AND operator in compound DisplayValue strings 
            //or "][" indicating multiple field codes to be evaluated -
            //reline control value, which contains nodes for Standard and Special
            //is an example where DisplayValue may contain two field codes

            //***REPLACING THESE CHARACTERS DOESN'T SEEM TO BE NECESSARY, AND DOES NOT WORK CORRECTLY WITH
            //***MORE COMPLEX EXPRESSIONS - LET EXPRESSION.EVALUATE DEAL WITH THIS IN THE NORMAL FASHION
            //xFieldCode = xFieldCode.Replace("^AND", LMP.StringArray.mpEndOfElement.ToString() + "[");
            //xFieldCode = xFieldCode.Replace("][", "]" + LMP.StringArray.mpEndOfElement.ToString() + "[");

            //create array of field codes for further pre-evaluation
            string[] xFieldCodes = xFieldCode.Split(LMP.StringArray.mpEndOfElement);
            StringBuilder oSB = new StringBuilder();

            //build an array list of values returned by Expression.Evaluate
            ArrayList oArrayList = new ArrayList();
            string xTemp = null;
            string[] aTemp = null;

            //for each configured field code, evaluate and return its string value
            //in the case of CI field codes, the string will be delimited by "; "
            //one member for each recipient
            for (int i = 0; i <= xFieldCodes.GetUpperBound(0); i++)
            {
                xTemp = xFieldCodes[i];
                xTemp = xTemp.Replace("; ", ";mp10Tempmp10 ");
                if (xTemp == "[TagValue]" || xTemp == "[MyTagValue]")
                {
                    xTemp = oVar.AssociatedContentControls[0].InnerText;
                }
                else
                {
                    xTemp = LMP.Architect.Oxml.XmlExpression.Evaluate(
                        xTemp, oVar.Segment, oVar.Segment.ForteDocument);
                }

                if (xTemp == null)
                    xTemp = "";

                //split value and add to arraylist
                xTemp = xTemp.Replace(";mp10Tempmp10 ", ";");
                xTemp = xTemp.Replace("mp10Tempmp10 ", " ");
                xTemp = xTemp.Replace("; ", "|");
                aTemp = xTemp.Split('|');
                oArrayList.Add(aTemp);
            }

            //go through the array list of field codes and match field code return values
            //In most cases aTemp will have only a single member.  For CI type field codes,
            //aTemp will have as many members as returned contacts.
            for (int i = 0; i < oArrayList.Count; i++)
            {
                string[] aItems = (string[])oArrayList[i];

                for (int j = 0; j < aItems.Length; j++)
                {
                    //delimit each record w/ semicolon, add
                    //commas between record fields, i.e, name, address; name2, address;
                    string xFormat = (j == aItems.Length - 1) ? "{0}; " : "{0}, ";

                    oSB.AppendFormat(xFormat, aItems[j]);
                }
            }

            string xValue = oSB.ToString();
            if (oVar.IsMultiValue)
            {
                //Replace value separator for display
                xValue = xValue.Replace(LMP.StringArray.mpEndOfValue.ToString(), "; ");
            }
            //eliminate double delimiters caused by empty values
            xValue = xValue.Replace(";;", ";");
            xValue = xValue.Replace("; ;", ";");
            xValue = xValue.Replace(",,", ",");
            xValue = xValue.Replace(", ,", ",");

            xValue = xValue.TrimEnd();
            xValue = xValue.TrimEnd(';');
            xValue = xValue.TrimStart(';');
            xValue = xValue.TrimStart(',');
            xValue = xValue.TrimStart();

            LMP.Benchmarks.Print(t0, oVar.Name);

            return xValue;
        }
    }
}

