﻿using System;
using System.Text;
using LMP.Architect.Oxml;
using System.Xml.Linq;

namespace TSG.WebControls
{
    public class DropdownList : ForteWebComboBase
    {
        public DropdownList(XmlVariable segmentVariable, XNamespace namespaceAttr) : base(segmentVariable, namespaceAttr)
        {
        }

        public override string ToHtml()
        {
            StringBuilder oSB = new StringBuilder();
            oSB.AppendFormat("<div id='cmb{0}' class='forte-control' data-forte-control-type='DropdownList' data-forte-variable='{1}' data-forte-segment='{2}' style='position: relative; height: {3}; z - index: 1;'>" +
                "<select name='MainCombobox' id='cmbMain{0}' width='100%' style='margin-top: 0px; position:absolute; top: 0px; ' xmlns='{6}' > {5} </select > </div >",
                Guid.NewGuid().ToString(), this.AssociatedVariableName, this.SegmentName, this.Height, this.ListRows, GetListItemHtml(this.ListName), this.Namespace);

            return oSB.ToString();
        }
    }
}
