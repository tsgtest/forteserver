﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace ForteServiceApp
{
    public partial class ForteServiceForm : Form
    {
        ServiceHost host;

        public ForteServiceForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
#if SSL
                Uri baseAddress = new Uri("https://localhost:4929/ForteService");
                WSHttpBinding binding = new WSHttpBinding(SecurityMode.Transport);

                // Create the ServiceHost.
                host = new ServiceHost(typeof(TSG.Forte.ForteService), baseAddress);

                // Enable metadata publishing.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpsGetEnabled = true;
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                host.Description.Behaviors.Add(smb);

                host.AddServiceEndpoint(typeof(TSG.Forte.IForteService), binding, "ForteService");
#else
                Uri baseAddress = new Uri("http://localhost:4929/ForteService");
                BasicHttpBinding binding = new BasicHttpBinding();

                // Create the ServiceHost.
                host = new ServiceHost(typeof(TSG.Forte.ForteService), baseAddress);

                // Enable metadata publishing.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                host.Description.Behaviors.Add(smb);

                //ServiceDebugBehavior sdb = new ServiceDebugBehavior();
                //sdb.IncludeExceptionDetailInFaults = true;
                //host.Description.Behaviors.Add(sdb);

                host.AddServiceEndpoint(typeof(TSG.Forte.IForteService), binding, "ForteService");
#endif
                host.Open();

                WriteMessage(string.Format("The service is ready at {0}", baseAddress));
                WriteMessage("TODO: Conditionalize fData to redirect server data functionality to use network DB - requires a subset of queries to have sql server analog.");
            }
            catch (System.Exception oE)
            {
                WriteMessage(LMP.Error.GetErrorDetail(oE));
            }
        }

        private void ForteServiceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // Close the ServiceHost.
                host.Close();
            }
            catch (System.Exception oE)
            {
                WriteMessage(LMP.Error.GetErrorDetail(oE));
            }
        }
        public void WriteMessage(string msg)
        {
            this.txtMsgs.Text += DateTime.Now.ToString("M/d/yy h:mm:ss tt") + " -  " + msg + "\r\n";
        }
    }
}
