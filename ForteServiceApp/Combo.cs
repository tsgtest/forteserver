﻿using System;
using System.Text;
using LMP.Architect.Oxml;
using System.Xml;
using System.Xml.Linq;
namespace TSG.WebControls
{
    public class Combo : ForteWebComboBase
    {
        public Combo(XmlVariable segmentVariable, XNamespace namespaceAttr) : base(segmentVariable, namespaceAttr)
        {
        }

        #region ForteWebControl overrides
        public override string ToHtml()
        {
            StringBuilder oSB = new StringBuilder();
            oSB.AppendFormat("<div id='cc{0}' class='forte-control' data-forte-control-type='Combo' data-forte-variable='{1}' data-forte-segment='{2}' style='position: relative; left: {3}; height: {4}; z - index: 1;'>" +
                "<input name = 'MainTextbox' id='txtMain{0}' type='text' value='{5}' class='forte-control-mlc-textarea' />" +
                "<input type='button' name='DropDownButton' value='v' id='btnDD{0}' onclick='ComboToggleDropdown(this.parentElement)' class='forte-control-mlc-button' />" +
                "<select name='MainListbox' id='lstMain{0}' size='{6}' onclick='ComboSelection(this.parentElement)' onblur='ComboCloseDropdown(this.parentElement)' class='forte-control-mlc-select' xmlns='{8}' > {7} </select > </div >",
                Guid.NewGuid().ToString(), this.AssociatedVariableName, this.SegmentName, this.Left == "" ? "0px" : this.Left, this.Height, this.Value, this.ListRows, GetListItemHtml(this.ListName), this.Namespace);
            return oSB.ToString();
        }
        #endregion
    }
}