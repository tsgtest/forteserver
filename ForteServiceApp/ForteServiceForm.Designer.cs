﻿namespace ForteServiceApp
{
    partial class ForteServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMsgs = new System.Windows.Forms.TextBox();
            this.txtCmd = new System.Windows.Forms.TextBox();
            this.lblCmd = new System.Windows.Forms.Label();
            this.btnSendCmd = new System.Windows.Forms.Button();
            this.lblMsgs = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtMsgs
            // 
            this.txtMsgs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMsgs.HideSelection = false;
            this.txtMsgs.Location = new System.Drawing.Point(3, 75);
            this.txtMsgs.Multiline = true;
            this.txtMsgs.Name = "txtMsgs";
            this.txtMsgs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMsgs.Size = new System.Drawing.Size(551, 328);
            this.txtMsgs.TabIndex = 4;
            // 
            // txtCmd
            // 
            this.txtCmd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCmd.Location = new System.Drawing.Point(3, 27);
            this.txtCmd.Name = "txtCmd";
            this.txtCmd.Size = new System.Drawing.Size(504, 20);
            this.txtCmd.TabIndex = 1;
            // 
            // lblCmd
            // 
            this.lblCmd.AutoSize = true;
            this.lblCmd.Location = new System.Drawing.Point(4, 11);
            this.lblCmd.Name = "lblCmd";
            this.lblCmd.Size = new System.Drawing.Size(57, 13);
            this.lblCmd.TabIndex = 0;
            this.lblCmd.Text = "Command:";
            // 
            // btnSendCmd
            // 
            this.btnSendCmd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendCmd.Location = new System.Drawing.Point(511, 25);
            this.btnSendCmd.Name = "btnSendCmd";
            this.btnSendCmd.Size = new System.Drawing.Size(40, 23);
            this.btnSendCmd.TabIndex = 2;
            this.btnSendCmd.Text = "Send";
            this.btnSendCmd.UseVisualStyleBackColor = true;
            // 
            // lblMsgs
            // 
            this.lblMsgs.AutoSize = true;
            this.lblMsgs.Location = new System.Drawing.Point(4, 59);
            this.lblMsgs.Name = "lblMsgs";
            this.lblMsgs.Size = new System.Drawing.Size(58, 13);
            this.lblMsgs.TabIndex = 3;
            this.lblMsgs.Text = "Messages:";
            // 
            // ForteServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 406);
            this.Controls.Add(this.lblMsgs);
            this.Controls.Add(this.btnSendCmd);
            this.Controls.Add(this.lblCmd);
            this.Controls.Add(this.txtCmd);
            this.Controls.Add(this.txtMsgs);
            this.Name = "ForteServiceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Forte Service";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ForteServiceForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCmd;
        private System.Windows.Forms.Label lblCmd;
        private System.Windows.Forms.Button btnSendCmd;
        private System.Windows.Forms.Label lblMsgs;
        public System.Windows.Forms.TextBox txtMsgs;
    }
}

