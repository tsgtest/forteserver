﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Security;

namespace TSG.Forte
{
#if SSL
    [ServiceContract(Name = "ForteService", Namespace = "http://TSG.Forte",ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
    [ServiceContract(Name = "ForteService", Namespace = "http://TSG.Forte")]
#endif
    public interface IForteService
    {
#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string CreateDocument(string id, string prefill, string emailAddress, string systemUserID);

#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetFoldersXml(int parentFolderID, string emailAddress, string systemUserID);

#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetFoldersJSON(int parentFolderID, string emailAddress, string systemUserID);

#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetFolderMembersJSON(int parentFolderID, string emailAddress, string systemUserID);
#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetSegmentTreeXml(string id, string prefill, string emailAddress, string systemUserID);
#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetListXml(string xListName, string emailAddress, string systemUserID);
#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetHelpText(string xHelpText, string emailAddress, string systemUserID);
#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetExternalDataSet(string id, string emailAddress, string systemUserID);
#if SSL
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
#else
        [OperationContract]
#endif
        string GetSegmentTreeHtml(string id, string prefill, TreeControlTypes treeControl, string emailAddress, string systemUserID);
    }
}
