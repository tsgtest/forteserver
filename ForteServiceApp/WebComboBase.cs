﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Threading.Tasks;
using LMP.Architect.Oxml;

namespace TSG.WebControls
{
    /// <summary>
    /// abstract base class for all Forte Web Combo-like controls
    /// </summary>
    public abstract class ForteWebComboBase: ForteWebControl
    {
        #region ************************Constructors*****************************
        public ForteWebComboBase(XmlVariable segmentVariable, XNamespace namespaceAttr) : base(segmentVariable, namespaceAttr)
        {
            string listName = this.ControlProperties["ListName"];
            if (string.IsNullOrEmpty(listName))
            {
                string listArray = this.ControlProperties["ListArray"];
                //TODO: populate this.ListArray
            }
            else
            {
                this.ListName = listName;
            }

            string listRows = this.ControlProperties["ListRows"];
            this.ListRows = string.IsNullOrEmpty(listRows) ? 12 : int.Parse(listRows);
        }
        #endregion

        #region ************************Methods*****************************
        protected string GetListItemHtml(string listName)
        {
            LMP.Data.Lists lists = LMP.Data.Application.GetLists(false);
            string listNameEval = XmlExpression.Evaluate(ListName, this.segmentVariable.Segment, this.segmentVariable.ForteDocument);
            LMP.Data.List list = (LMP.Data.List)lists.ItemFromName(listNameEval);
            byte valueColumn = 1;
            byte displayColumn = 1;
            if (list.ColumnCount == 2)
            {
                valueColumn = 1;
                displayColumn = 2;
            }

            StringBuilder sb = new StringBuilder();

            for (int l = 0; l < list.ItemCount; l++)
            {
                XElement newOption = new XElement(this.Namespace + "option", 
                    new XAttribute("value", list.ItemFromIndex(l, valueColumn)))
                { Value = list.ItemFromIndex(l, displayColumn) };
                sb.AppendLine(newOption.ToString());
            }
            return sb.ToString();
        }
        #endregion

        #region ************************Properties*****************************
        public string ListName { get; set; }
        public int ListRows { get; set; }
        public string[,] ListArray { get; set; }
        #endregion
    }
}
