﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TSG
{
    //[ServiceContract(ProtectionLevel=System.Net.Security.ProtectionLevel.EncryptAndSign)]
    [ServiceContract()]
    public interface IForteServer
    {

        //[OperationContract(ProtectionLevel=System.Net.Security.ProtectionLevel.EncryptAndSign)]
        [OperationContract]
        string[] ProcessInstructionSet(string instructionSet);
    }
}
