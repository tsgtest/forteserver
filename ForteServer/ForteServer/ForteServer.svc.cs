﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Web.Script.Services;

namespace TSG
{
    [ScriptService]
    public class ForteServer : IForteServer
    {
        /*
         * <ForteInstructions>
         * <ForteInstruction type=CreateDocument>
         *      <User>dfisherman</User>
	     *      <ID>3039</ID>
	     *      <Prefill>
         *          <Authors>2143<\Authors>
                    <AuthorInitials>LAA</AuthorInitials>
                    <CC><FULLNAME Index='1'>Jerry Wolfe</FULLNAME></CC>
                    <BCC><FULLNAME Index='1'>Linda J. Sackett</FULLNAME></BCC>
                    <ClientMatterNumber>53001.001</ClientMatterNumber>
                    <ClosingPhrase>Sincerely,</ClosingPhrase>
                    <DateFormat>MMMM d, yyyy</DateFormat>
                    <DeliveryPhrases>By Certified Mail
                    Return Receipt Requested</DeliveryPhrases>
                    <Enclosures>Enclosure</Enclosures>
                    <HeaderDeliveryPhrases>True</HeaderDeliveryPhrases>
                    <IncludeAdmittedIn>False</IncludeAdmittedIn>
                    <IncludeEMail>True</IncludeEMail>
                    <IncludeFax>False</IncludeFax>
                    <IncludeName>True</IncludeName>
                    <IncludePhone>True</IncludePhone>
                    <LetterheadIncludeTitle>False</LetterheadIncludeTitle>
                    <Page2HeaderText>Ms. Laura Halliday
                    Ms. Valerie Melville</Page2HeaderText>
                    <ReLine>Subject line</ReLine>
                    <Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1'>Ms. Laura Halliday</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1'>Project Manager</TITLE><COMPANY Index='1'>Legal MacPac</COMPANY><COREADDRESS Index='1'>1430 Judah Street, #2
                    San Francisco, CA  94122</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2'>Ms. Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2'>Product and Client Services Manager</TITLE><COMPANY Index='2'>The Sackett Group, Inc.</COMPANY><COREADDRESS Index='2'>7367 Los Brazos
                    San Diego, CA  92127</COREADDRESS></Recipients>
                    <Salutation>Dear Laura and Valerie:</Salutation>
                    <AuthorName>Louis A. Armstrong</AuthorName>
                    <IncludeFirmName>True</IncludeFirmName>
                    <IncludeTitle>True</IncludeTitle>
                    <TypistInitials>ljs</TypistInitials>
         *      </Prefill>
	     *      <EmailAddress>DFisherman@thesackettgroup.com</EmailAddress>
	     *      <OutputFormat>docx</OutputFormat>
         *      <Automated>true</Automated>
         * </ForteInstruction>
         * <ForteInstruction type=GetFoldersXml>
         *      <User>dfisherman</User>
	     *      <ParentFolderID>124</ID>
	     *      <EmailAddress>DFisherman@thesackettgroup.com</EmailAddress>
         * </ForteInstruction>
         * </ForteInstructions>
        */

        [ScriptMethod]
        public string[] ProcessInstructionSet(string instructionSet)
        {
            //parse instruction
            XmlDocument oXml = new XmlDocument();
            oXml.LoadXml(instructionSet);
            XmlNodeList instructions = oXml.DocumentElement.GetElementsByTagName("ForteInstruction");

            if (instructions != null)
            {
                string[] aRet = new string[instructions.Count];
                int i = 0;

                //aRet[0] = "ProcessInstructionSet completed successfully.";
                //return aRet;

                foreach (XmlNode instruction in instructions)
                {
                    string instructionType = instruction.SelectSingleNode("@type").Value.ToString();

                    switch (instructionType.ToLower())
                    {
                        case "createdocument":
                            aRet[i] = CreateDocument(instruction);
                            break;
                        case "getuixml":
                            aRet[i] = GetUIXml(instruction);
                            break;
                        case "getuihtml":
                            aRet[i] = GetSegmentTreeHtml(instruction);
                            break;
                        case "getfoldersxml":
                            aRet[i] = GetFoldersXml(instruction);
                            break;
                        case "getfoldermembersjson":
                            aRet[i] = GetFolderMembersJSON(instruction);
                            break;
                        case "getfoldersjson":
                            aRet[i] = GetFoldersJSON(instruction);
                            break;
                        case "getfoldersegmentsxml":
                            aRet[i] = GetFolderSegmentsXml(instruction);
                            break;
                        case "getlistxml":
                            aRet[i] = GetListXml(instruction);
                            break;
                        case "gethelptext":
                            aRet[i] = GetHelpText(instruction);
                            break;
                        case "getexternaldataset":
                            aRet[i] = GetExternalDataSet(instruction);
                            break;
                    }

                    i++;
                }
                return aRet;
            }
            else
                return null;
        }

        //public string CreateDocument(string id, string prefill, string emailAddress)
        //{
        //    TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
        //    return oForte.CreateDocument(id, prefill, emailAddress);
        //}

        private string GetExternalDataSet(XmlNode instruction)
        {
            StringBuilder oSB = new StringBuilder();
            string xDataSetID = instruction.SelectSingleNode(@"//ID").InnerText;
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;
            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            return oForte.GetExternalDataSet(xDataSetID, xEmail, xUser);
        }

        private string GetUIXml(XmlNode instruction)
        {
            //returns the Xml for building a UI in any client application -
            //xml is platform independent, allowing the client application
            //to define the implementation of the UI

            string xID = instruction.SelectSingleNode(@"//ID").InnerText;
            XmlNode oPrefill = instruction.SelectSingleNode(@"//Prefill");

            StringBuilder oSB = new StringBuilder();
            string xPrefill = oPrefill.InnerXml;
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;
            string xOutputFormat = instruction.SelectSingleNode(@"//OutputFormat").InnerText;
            //user id as lead author to retrieve default values
            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            return oForte.GetSegmentTreeXml(xID, xPrefill, xEmail, xUser);

        }
        private string GetListXml(XmlNode instruction)
        {
            //returns the Xml for building a UI in any client application -
            //xml is platform independent, allowing the client application
            //to define the implementation of the UI

            string xListName = instruction.SelectSingleNode(@"//ListName").InnerText;

            StringBuilder oSB = new StringBuilder();
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;
            string xOutputFormat = instruction.SelectSingleNode(@"//OutputFormat").InnerText;
            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            return oForte.GetListXml(xListName, xEmail, xUser);

        }
        private string GetHelpText(XmlNode instruction)
        {
            //returns the Xml for building a UI in any client application -
            //xml is platform independent, allowing the client application
            //to define the implementation of the UI

            string xHelpText = instruction.SelectSingleNode(@"//HelpText").InnerText;

            StringBuilder oSB = new StringBuilder();
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;
            string xOutputFormat = instruction.SelectSingleNode(@"//OutputFormat").InnerText;
            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            return oForte.GetHelpText(xHelpText, xEmail, xUser);

        }
        private string GetSegmentTreeHtml(XmlNode instruction)
        {
            /*return HTML for building a UI in a browser - 
             *instruction parameters:
             *segment id
             *user id
             *browser id
             *
             * implementation - retrieve ui data from segment xml,
             * using user id as lead author to retrieve default values - 
             * massage xml into brower-specific HTML -
             * we can start with one browser and expand after
             * */
            string xID = instruction.SelectSingleNode(@"//ID").InnerText;
            XmlNode oPrefill = instruction.SelectSingleNode(@"//Prefill");
            string xFormat = instruction.SelectSingleNode(@"//Format").InnerText;
            if (xFormat == "")
                xFormat = "0";
            StringBuilder oSB = new StringBuilder();
            string xPrefill = oPrefill.InnerXml;
            TSG.Forte.ForteService.TreeControlTypes iTreeType = (TSG.Forte.ForteService.TreeControlTypes)Int32.Parse(xFormat);
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;
            string xOutputFormat = instruction.SelectSingleNode(@"//OutputFormat").InnerText;
            //user id as lead author to retrieve default values
            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            return oForte.GetSegmentTreeHtml(xID, xPrefill,  iTreeType, xEmail, xUser);
        }

        private string GetFoldersXml(XmlNode instruction)
        {
            /*return xml that describes part of the folder
             * tree accessible to users - allows a client
             * application to implement content manager-like
             * functionality - we will leave it to the client
             * application to determine the extent of functionality
             * offered
             * instruction parameters:
             * parent folder id
             * user id
             */

            int iParentFolderID = int.Parse(instruction.SelectSingleNode(@"//ParentFolderID").InnerText);
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;

            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            return oForte.GetFoldersXml(iParentFolderID, xEmail, xUser);
        }

        private string GetFoldersJSON(XmlNode instruction)
        {
            /*return xml that describes part of the folder
             * tree accessible to users - allows a client
             * application to implement content manager-like
             * functionality - we will leave it to the client
             * application to determine the extent of functionality
             * offered
             * instruction parameters:
             * parent folder id
             * user id
             */

            int iParentFolderID = int.Parse(instruction.SelectSingleNode(@"//ParentFolderID").InnerText);
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;

            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            string ret = oForte.GetFoldersJSON(iParentFolderID, xEmail, xUser);
            return ret;
        }

        private string GetFolderMembersJSON(XmlNode instruction)
        {
            /*return xml that describes part of the folder
             * tree accessible to users - allows a client
             * application to implement content manager-like
             * functionality - we will leave it to the client
             * application to determine the extent of functionality
             * offered
             * instruction parameters:
             * parent folder id
             * user id
             */

            int iParentFolderID = int.Parse(instruction.SelectSingleNode(@"//ParentFolderID").InnerText);
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;

            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            string ret = oForte.GetFolderMembersJSON(iParentFolderID, xEmail, xUser);
            return ret;
        }

        private string GetFolderSegmentsXml(XmlNode instruction)
        {
            /*return xml that describes the segments in a folder -
             * allows client application to display folder segments
             * instruction parameters:
             * folder id
             * user id
             * */

            return null;
        }

        private string CreateDocument(XmlNode instruction)
        {
            string xID = instruction.SelectSingleNode(@"//ID").InnerText;
            XmlNode oPrefill = instruction.SelectSingleNode(@"//Prefill");
            
            StringBuilder oSB = new StringBuilder();
            string xPrefill = oPrefill.InnerXml;
            string xUser = instruction.SelectSingleNode(@"//User").InnerText;
            string xEmail = instruction.SelectSingleNode(@"//EmailAddress").InnerText;
            string xOutputFormat = instruction.SelectSingleNode(@"//OutputFormat").InnerText;

            TSG.Forte.ForteService.ForteServiceClient oForte = new TSG.Forte.ForteService.ForteServiceClient();
            return oForte.CreateDocument(xID, xPrefill, xEmail, xUser);
        }
    }

    static class ThreadPoolTimeoutWorkaround
    {
        static ManualResetEvent s_dummyEvent;
        static RegisteredWaitHandle s_registeredWait;

        public static void DoWorkaround()
        {
            Console.WriteLine("Doing Workaround.");

            // Create an event that is never set
            s_dummyEvent = new ManualResetEvent(false);

            // Register a wait for the event, with a periodic timeout. This causes callbacks
            // to be queued to an IOCP thread, keeping it alive3
            s_registeredWait = ThreadPool.RegisterWaitForSingleObject(
                s_dummyEvent,
                (a, b) =>
                {
                    // Do nothing
                },
                null,
                1000,
                false);
        }
    }

}
