﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMP.Architect.Oxml.Server;

namespace ForteServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Text = "";
                DateTime t0 = DateTime.Now;

                LMP.Architect.Oxml.XmlSegment oSegment = XmlSegmentInsertion.Insert("3039", null, LMP.Architect.Base.Segment.InsertionLocations.InsertInNewDocument,
                    LMP.Architect.Base.Segment.InsertionBehaviors.Default, null, true, true, null, false, false, false, null);

                LMP.Benchmarks.Print(t0);
                this.Text = "Done";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
    }
}
