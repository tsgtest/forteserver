﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ForteServer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Start();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE, "Could not initialize the Forte Server.");
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        /// <summary>
        /// initializes a MacPac session
        /// </summary>
        private static bool Start()
        {
            try
            {
                DateTime t0 = DateTime.Now;
                LMP.Data.Application.WriteDBLog("Session.Start"); //GLOG 7546

                //install synched db if one exists
                //GLOG 4738: If ForteSync.mdb cannot be installed, 
                //display message and continue
                try
                {
                    LMP.Data.Application.InstallUpdatedDBIfNecessary();
                }
                catch { }

                LMP.Data.Application.RenameUserDBIfSpecified();

                //get current system user name
                string xSystemUserID = "Server";

                bool bAdminMode = false;

                //get required location of local admin file
                string xAdminDB = LMP.Data.Application.WritableDBDirectory + 
                    @"\" + LMP.Data.Application.AdminDBName; //JTS 3/28/13

                LMP.Trace.WriteNameValuePairs("xAdminDB", xAdminDB,
                    "xSystemUserID", xSystemUserID);

                //another session of Forte may have already been
                //started - in the case of Word 2003, clicking a menu
                //item starts a session - get value that specifies
                //whether that session is in admin mode or not
                string xExternalLogonAsAdmin =
                    LMP.Registry.GetCurrentUserValue(
                        @"Software\The Sackett Group\Deca", "AdminMode");

                if (!string.IsNullOrEmpty(xExternalLogonAsAdmin))
                {
                    //previous session was started - get whether
                    //in admin mode or not
                    bAdminMode = xExternalLogonAsAdmin == "1";
                }
                else
                {
                    //GLOG : 7998 : ceh
                    //if (LMP.Data.Application.IsAdminDB(xAdminDB) &&
                    //    (Person.HasAdminAccess(xSystemUserID))
                    if (LMP.Data.Application.IsAdminDB(xAdminDB))
                    {
                        //if (File.Exists(LMP.Data.Application.WritableDBDirectory + "\\" + LMP.Data.Application.UserDBName)) //JTS 3/28/13
                        //{
                        //    //admin db exists and current system user is an admin- 
                        //    //prompt for user or admin mode
                        //    //TODO: spice up this dialog box
                        //    AdminLogonForm oLoginForm = new AdminLogonForm();
                        //    oLoginForm.LoginChoice = AdminLogonForm.LoginChoices.Administrator;
                        //    oLoginForm.ShowDialog();
                        //    oLoginForm.Hide();

                        //    System.Windows.Forms.Application.DoEvents();

                        //    //set admin mode based on user choice
                        //    bAdminMode = oLoginForm.LoginChoice ==
                        //        AdminLogonForm.LoginChoices.Administrator;

                        //    oLoginForm.Close();
                        //}
                        //else
                        //{
                            bAdminMode = true;
                        //}
                    }

                    //mark in registry whether this session
                    //is in admin mode or not
                    LMP.Registry.SetCurrentUserValue(
                        @"Software\The Sackett Group\Deca", "AdminMode",
                        bAdminMode ? "1" : "");
                }

                //GLOG #2879 - deal gracefully with lack of guest support - DF
                try
                {
                    Initialize(xSystemUserID);
                }
                catch (System.Exception oE)
                {
                    LMP.Trace.WriteError("Could not initialize Forte Server.  " + LMP.Error.GetErrorDetail(oE));
                    throw oE;
                }

                ////10-14-11 (dm) - moved line up for greater scope
                //FirmApplicationSettings oSettings = Session.CurrentUser.FirmSettings;

                //if (LMP.Data.Application.LastSyncFailed)
                //{
                //    //reset last sync failed key
                //    LMP.Registry.SetCurrentUserValue(
                //        LMP.Data.ForteConstants.mpSyncRegKey, "LastSyncFailed", "0");

                //    //get mail server parameters from app settings
                //    //JTS 12/17/09: Don't attempt to send message unless mail settings are configured
                //    if (oSettings.MailHost != "" && (oSettings.MailUseNetworkCredentialsToLogin ||
                //            (oSettings.MailUserID != "" && oSettings.MailPassword != "")))
                //    {
                //        MailServerParameters oParams;
                //        oParams.UseNetworkCredentialsToLogin = oSettings.MailUseNetworkCredentialsToLogin;
                //        oParams.Host = oSettings.MailHost;
                //        oParams.UserID = oSettings.MailUserID;
                //        oParams.Password = oSettings.MailPassword;
                //        oParams.UseSsl = oSettings.MailUseSsl;
                //        oParams.Port = oSettings.MailPort;

                //        try
                //        {
                //            //send message
                //            LMP.OS.SendMessage(Session.CurrentUser.PersonObject.GetPropertyValue("EMail").ToString(),
                //                LMP.ComponentProperties.ProductName + " User Synchronization Failed.",
                //                LMP.ComponentProperties.ProductName + " user synchronization failed for " + Environment.UserName +
                //                ".  You can check that user's SyncErrors.log in the " + LMP.ComponentProperties.ProductName + " data directory for more information.",
                //                oParams, null, oSettings.MailAdminEMail);
                //        }
                //        catch
                //        {
                //            //Don't stop initialization if there's a problem sending Admin e-mail
                //        }
                //    }
                //}

                if (bAdminMode)
                    //update the admin db structure if necessary
                    LMP.Data.Application.UpdateLocalDBStructureIfNecessary(true);
                else
                    //update the user db structure if necessary
                    LMP.Data.Application.UpdateLocalDBStructureIfNecessary(false);

                //Clean up Local DB
                LMP.Data.Application.DeleteOrphanedUserFolderMembers();

                ////pass client's tag prefix id and encryption password to mpCOM
                //string xTagPrefixID = LMP.Data.Application.CurrentClientTagPrefixID;
                //string xEncryptionPassword = LMP.Data.Application.EncryptionPassword;
                //fCOM.cApplication oCOM = new fCOM.cApplication();
                //oCOM.SetClientMetadataValues(xTagPrefixID, xEncryptionPassword);

                ////determine whether to disable encryption for this session -
                ////two keys are required - one from the registry, the other from
                ////the metadata table
                //string xRegPassword = LMP.Registry.GetMacPac10Value("DisableEncryption");

                //oCOM.DisableEncryption(xRegPassword.ToLower() == "miller" &&
                //    LMP.Data.Application.DisableEncryptionPassword.ToLower() == "fisherman");

                ////10-14-11 - set Base64 encoding
                //oCOM.SetBase64Encoding(oSettings.UseBase64Encoding);

                ////update styles in Normal.dot if necessary and allowed
                //if (!bAdminMode && Session.CurrentUser.FirmSettings.AllowNormalStyleUpdates &&
                //    (Session.LastNormalStylesUpdate < LMP.Data.Application.GetFirmNormalLastEditTime()))
                //{
                //    //update styles in local normal.dot for current user
                //    LMP.MacPac.Application.ResetNormalStyles();
                //}
                ////GLOG 6360: Create separate metadata value for each user
                //if (!bAdminMode && LMP.Data.Application.GetMetadata("DefaultUserFoldersCreated" + Session.CurrentUser.ID.ToString()) != "True")
                //{
                //    //create default user folders if none already exist
                //    UserFolders oFolders = new UserFolders(Session.CurrentUser.ID);

                //    //creation has not yet been attempted - we try once, so that if
                //    //users deleted the defaults, no second attempt would be made

                //    // GLOG : 5117 : CEH
                //    if (!LMP.MacPac.MacPacImplementation.IsToolkit && oFolders.Count == 0)
                //    {
                //        string xDefaultUserFolders = "";
                //        string xDefaultDescriptions = "";

                //        if (Session.CurrentUser.FirmSettings.DefaultUserFolders.Length > 0)
                //        {
                //            //load Firm Settings' default user folders
                //            string xTemp = Session.CurrentUser.FirmSettings.DefaultUserFolders;
                //            string[] axTemp = xTemp.Split(';');

                //            for (int i = 0; i < axTemp.Length; i = i + 2)
                //            {
                //                xDefaultUserFolders = xDefaultUserFolders + axTemp[i] + ";";
                //                xDefaultDescriptions = xDefaultDescriptions + axTemp[i + 1] + ";";
                //            }

                //            xDefaultUserFolders = xDefaultUserFolders.TrimEnd(new char[] { ';' });
                //            xDefaultDescriptions = xDefaultDescriptions.TrimEnd(new char[] { ';' });
                //        }
                //        else
                //        {
                //            //load default user folders
                //            xDefaultUserFolders = LMP.Resources.GetLangString("Default_UserFolder_Names");
                //            xDefaultDescriptions = LMP.Resources.GetLangString("Default_UserFolder_Descriptions");
                //        }

                //        string[] axUserFolders = xDefaultUserFolders.Split(';');
                //        string[] axDescriptions = xDefaultDescriptions.Split(';');

                //        for (int i = 0; i < axUserFolders.Length; i++)
                //        {

                //            UserFolder oFolder;
                //            //GLOG 4586: Make sure a unique User Item ID has been assigned before
                //            //saving the new UserFolder object.  There could be a conflict here
                //            //if time since creation of last item is small enough
                //            bool bDuplicateID = false;
                //            do
                //            {
                //                oFolder = (UserFolder)oFolders.Create(Session.CurrentUser.ID);
                //                try
                //                {
                //                    bDuplicateID = (oFolders.ItemFromID(oFolder.ID) != null);
                //                }
                //                catch
                //                {
                //                    bDuplicateID = false;
                //                }
                //            } while (bDuplicateID);
                //            oFolder.Name = axUserFolders[i];
                //            oFolder.Description = axDescriptions[i];
                //            oFolders.Save(oFolder);
                //        }
                //    }
                //    //GLOG 6360
                //    LMP.Data.Application.SetMetadata("DefaultUserFoldersCreated" + Session.CurrentUser.ID.ToString(), "True");
                //}

                LMP.Benchmarks.Print(t0);

                return true;
            }
            catch (System.Exception oE)
            {
                throw new LMP.Exceptions.SessionException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeSession"), oE);
            }
            finally
            {
                ////GLOG 6655: Prevent repeated attempts in this session
                //m_bInitializationAttempted = true;
            }
        }
        private static void Initialize(string xSystemUserID)
        {
            try
            {
                DateTime t0 = DateTime.Now;

                //GLOG 7898 (dm) - set corresponding property in fBase - an inelegant solution to the
                //MacPacImplementation class needing to remain at the lowest point in the hierarchy
                LMP.MacPac.MacPacImplementation.IsAdminMode = true;

                bool bRet = LMP.Data.Application.Login(xSystemUserID, true);

                if (bRet)
                {
                    //login was successful - set up session
                    //TODO: softcode culture to pull from user application settings
                    LMP.Resources.SetCulture(1033);
                }

                LMP.Benchmarks.Print(t0);
            }
            catch (LMP.Exceptions.InvalidUserException oE)
            {
                throw oE;
            }
            catch (System.Exception oE)
            {
                    //GLOG 5857: Release Forte.mdb
                    LMP.Data.Application.Logout();

                    //repair dbs
                    LMP.Data.Application.RepairUserDBs();
                throw new LMP.Exceptions.SessionException(
                    LMP.Resources.GetLangString("Error_CouldNotInitializeSession"), oE);
            }
            finally
            {
            }
        }
    }
}
