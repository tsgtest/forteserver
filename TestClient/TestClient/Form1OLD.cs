﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* <ForteInstructions>
            *    <ForteInstruction type=CreateDocument>
            *      <ID>3039</ID>
            *      <Prefill>
                       <Authors>2143<\Authors>
                       <AuthorInitials>LAA</AuthorInitials>
                       <CC><FULLNAME Index='1'>Jerry Wolfe</FULLNAME></CC>
                       <BCC><FULLNAME Index='1'>Linda J. Sackett</FULLNAME></BCC>
                       <ClientMatterNumber>53001.001</ClientMatterNumber>
                       <ClosingPhrase>Sincerely,</ClosingPhrase>
                       <DateFormat>MMMM d, yyyy</DateFormat>
                       <DeliveryPhrases>By Certified Mail
                       Return Receipt Requested</DeliveryPhrases>
                       <Enclosures>Enclosure</Enclosures>
                       <HeaderDeliveryPhrases>True</HeaderDeliveryPhrases>
                       <IncludeAdmittedIn>False</IncludeAdmittedIn>
                       <IncludeEMail>True</IncludeEMail>
                       <IncludeFax>False</IncludeFax>
                       <IncludeName>True</IncludeName>
                       <IncludePhone>True</IncludePhone>
                       <LetterheadIncludeTitle>False</LetterheadIncludeTitle>
                       <Page2HeaderText>Ms. Laura Halliday
                       Ms. Valerie Melville</Page2HeaderText>
                       <ReLine>Subject line</ReLine>
                       <Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1'>Ms. Laura Halliday</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1'>Project Manager</TITLE><COMPANY Index='1'>Legal MacPac</COMPANY><COREADDRESS Index='1'>1430 Judah Street, #2
                       San Francisco, CA  94122</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2'>Ms. Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2'>Product and Client Services Manager</TITLE><COMPANY Index='2'>The Sackett Group, Inc.</COMPANY><COREADDRESS Index='2'>7367 Los Brazos
                       San Diego, CA  92127</COREADDRESS></Recipients>
                       <Salutation>Dear Laura and Valerie:</Salutation>
                       <AuthorName>Louis A. Armstrong</AuthorName>
                       <IncludeFirmName>True</IncludeFirmName>
                       <IncludeTitle>True</IncludeTitle>
                       <TypistInitials>ljs</TypistInitials>
            *      </Prefill>
            *      <EmailAddress>DFisherman@thesackettgroup.com</EmailAddress>
            *      <OutputFormat>docx</OutputFormat>
            *   </ForteInstruction>
            * </ForteInstruction> */

            try
            {
                string x = this.Text;
                this.Text = "Processing.";
                this.richTextBox1.Text = "";

                //add client functionality to Word, which will allow users to select segment and prefill, then send to server
                TSG.Forte.ForteServer.ForteServerClient oServer = new TSG.Forte.ForteServer.ForteServerClient();
                string instruction = "<ForteInstructions><ForteInstruction type=\"CreateDocument\"><ID>6707</ID><Prefill><Authors>2143</Authors><AuthorInitials>LAA</AuthorInitials><CC><FULLNAME Index=\"1\">Jerry Wolfe</FULLNAME></CC><BCC><FULLNAME Index=\"1\">Linda J. Sackett</FULLNAME></BCC><ClientMatterNumber>53001.001</ClientMatterNumber><ClosingPhrase>Sincerely,</ClosingPhrase><DateFormat>MMMM d, yyyy</DateFormat><DeliveryPhrases>By Certified Mail Return Receipt Requested</DeliveryPhrases><Enclosures>Enclosure</Enclosures><HeaderDeliveryPhrases>True</HeaderDeliveryPhrases><IncludeAdmittedIn>False</IncludeAdmittedIn><IncludeEMail>True</IncludeEMail><IncludeFax>False</IncludeFax><IncludeName>True</IncludeName><IncludePhone>True</IncludePhone><LetterheadIncludeTitle>True</LetterheadIncludeTitle><Page2HeaderText>Ms. Laura Halliday Ms. Valerie Melville</Page2HeaderText><ReLine>Subject line</ReLine><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index=\"1\">Ms. Laura Halliday</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"1\">Project Manager</TITLE><COMPANY Index=\"1\">Legal MacPac</COMPANY><COREADDRESS Index=\"1\">1430 Judah Street, #2 San Francisco, CA  94122</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index=\"2\">Ms. Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"2\">Product and Client Services Manager</TITLE><COMPANY Index=\"2\">The Sackett Group, Inc.</COMPANY><COREADDRESS Index=\"2\">7367 Los Brazos San Diego, CA  92127</COREADDRESS></Recipients><Salutation>Dear Laura and Valerie:</Salutation><AuthorName>Louis A. Armstrong</AuthorName><IncludeFirmName>True</IncludeFirmName><IncludeTitle>True</IncludeTitle><TypistInitials>ljs</TypistInitials></Prefill><EmailAddress>DFisherman@thesackettgroup.com</EmailAddress><OutputFormat>docx</OutputFormat> </ForteInstruction></ForteInstructions>";
                //string instruction = "<ForteInstructions><ForteInstruction type=\"CreateDocument\"><ID>6707</ID><Prefill><IncludeTitle>true</IncludeTitle><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1' UNID='0'>Daniel Fisherman</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1' UNID='0'>Developer</TITLE><COMPANY Index='1' UNID='0'>TSG</COMPANY><COREADDRESS Index='1' UNID='0'>my address&#13;&#10;my city, my state my zip</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2' UNID='0'>Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2' UNID='0'>Manager</TITLE><COMPANY Index='2' UNID='0'>TSG</COMPANY><COREADDRESS Index='2' UNID='0'>her address&#13;&#10;her city, her state her zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString></Recipients><UseTableFormat>false</UseTableFormat><Reline><Standard Format='0' Default='Alternate Insurance'>this is the reline</Standard><Special Format='0' Default='Alternate Insurance'><Label Index='1'>Claimant</Label><Value Index='1'>a</Value><Label Index='2'>Date of Claim</Label><Value Index='2'>b</Value><Label Index='3'>Company Name</Label><Value Index='3'>c</Value><Label Index='4'>Action Number</Label><Value Index='4'>d</Value></Special></Reline><Subject>This is the subject line</Subject><Agreed>true</Agreed><DateFormat>MMMM dd, yyyy</DateFormat><ExecuteonBlock>true</ExecuteonBlock><TitleProperty>doc title</TitleProperty><CustomTitle>custom doc title</CustomTitle><DocumentVariableValue>dv custom 1 var text</DocumentVariableValue><SetLanguage>true</SetLanguage><FontName>Calibri</FontName><FontSize>12.0</FontSize><BodyTextFirstLineIndent>0.50</BodyTextFirstLineIndent><BodyTextAlignment>0</BodyTextAlignment><AuthorName>Daniel C. Fisherman</AuthorName><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors></Prefill><EmailAddress>momshead@gmail.com</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                string xText = oServer.ProcessInstructionSet(instruction);
                string xDir = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\The Sackett Group\Deca", "SaveDirectoryUrl", "C:\\").ToString();
                string xFileName = xDir + System.Guid.NewGuid().ToString() + ".docx";

                using (FileStream oFS = File.Create(xFileName))
                {
                    byte[] b = Convert.FromBase64String(xText);
                    oFS.Write(b, 0, b.Length);
                }

                this.Text = x;
                this.richTextBox1.Text = xText;
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
