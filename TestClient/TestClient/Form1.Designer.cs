﻿namespace TestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.lstRequest = new System.Windows.Forms.ListBox();
            this.lstSampleDataSet = new System.Windows.Forms.ListBox();
            this.lblRequest = new System.Windows.Forms.Label();
            this.lblSampleDataSet = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblArg1 = new System.Windows.Forms.Label();
            this.lblArg2 = new System.Windows.Forms.Label();
            this.lblArg3 = new System.Windows.Forms.Label();
            this.txtArg1 = new System.Windows.Forms.TextBox();
            this.txtArg2 = new System.Windows.Forms.TextBox();
            this.txtArg3 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(176, 36);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(592, 421);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 382);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 28);
            this.button2.TabIndex = 2;
            this.button2.Text = "&Execute";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lstRequest
            // 
            this.lstRequest.FormattingEnabled = true;
            this.lstRequest.Items.AddRange(new object[] {
            "CreateDocument",
            "GetUIXml",
            "GetUIHtml (native)",
            "GetUIHtml (jstree)",
            "GetUIHtml (fancytree)",
            "GetFoldersXML",
            "GetFolderMembersJSON",
            "GetListXml",
            "GetHelpText",
            "GetExternalDataSet"});
            this.lstRequest.Location = new System.Drawing.Point(12, 36);
            this.lstRequest.Name = "lstRequest";
            this.lstRequest.Size = new System.Drawing.Size(147, 173);
            this.lstRequest.TabIndex = 3;
            this.lstRequest.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // lstSampleDataSet
            // 
            this.lstSampleDataSet.FormattingEnabled = true;
            this.lstSampleDataSet.Location = new System.Drawing.Point(15, 266);
            this.lstSampleDataSet.Name = "lstSampleDataSet";
            this.lstSampleDataSet.Size = new System.Drawing.Size(144, 17);
            this.lstSampleDataSet.TabIndex = 4;
            this.lstSampleDataSet.Visible = false;
            // 
            // lblRequest
            // 
            this.lblRequest.AutoSize = true;
            this.lblRequest.Location = new System.Drawing.Point(12, 22);
            this.lblRequest.Name = "lblRequest";
            this.lblRequest.Size = new System.Drawing.Size(50, 13);
            this.lblRequest.TabIndex = 5;
            this.lblRequest.Text = "Request:";
            // 
            // lblSampleDataSet
            // 
            this.lblSampleDataSet.AutoSize = true;
            this.lblSampleDataSet.Location = new System.Drawing.Point(12, 252);
            this.lblSampleDataSet.Name = "lblSampleDataSet";
            this.lblSampleDataSet.Size = new System.Drawing.Size(90, 13);
            this.lblSampleDataSet.TabIndex = 6;
            this.lblSampleDataSet.Text = "Sample Data Set:";
            this.lblSampleDataSet.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(173, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Return Values:";
            // 
            // lblArg1
            // 
            this.lblArg1.AutoSize = true;
            this.lblArg1.Location = new System.Drawing.Point(16, 251);
            this.lblArg1.Name = "lblArg1";
            this.lblArg1.Size = new System.Drawing.Size(64, 13);
            this.lblArg1.TabIndex = 9;
            this.lblArg1.Text = "Argument 1:";
            // 
            // lblArg2
            // 
            this.lblArg2.AutoSize = true;
            this.lblArg2.Location = new System.Drawing.Point(14, 292);
            this.lblArg2.Name = "lblArg2";
            this.lblArg2.Size = new System.Drawing.Size(64, 13);
            this.lblArg2.TabIndex = 10;
            this.lblArg2.Text = "Argument 2:";
            // 
            // lblArg3
            // 
            this.lblArg3.AutoSize = true;
            this.lblArg3.Location = new System.Drawing.Point(14, 331);
            this.lblArg3.Name = "lblArg3";
            this.lblArg3.Size = new System.Drawing.Size(64, 13);
            this.lblArg3.TabIndex = 11;
            this.lblArg3.Text = "Argument 3:";
            // 
            // txtArg1
            // 
            this.txtArg1.Location = new System.Drawing.Point(12, 266);
            this.txtArg1.Name = "txtArg1";
            this.txtArg1.Size = new System.Drawing.Size(147, 20);
            this.txtArg1.TabIndex = 12;
            // 
            // txtArg2
            // 
            this.txtArg2.Location = new System.Drawing.Point(12, 307);
            this.txtArg2.Name = "txtArg2";
            this.txtArg2.Size = new System.Drawing.Size(147, 20);
            this.txtArg2.TabIndex = 13;
            // 
            // txtArg3
            // 
            this.txtArg3.Location = new System.Drawing.Point(12, 346);
            this.txtArg3.Name = "txtArg3";
            this.txtArg3.Size = new System.Drawing.Size(147, 20);
            this.txtArg3.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 469);
            this.Controls.Add(this.txtArg3);
            this.Controls.Add(this.txtArg2);
            this.Controls.Add(this.txtArg1);
            this.Controls.Add(this.lblArg3);
            this.Controls.Add(this.lblArg2);
            this.Controls.Add(this.lblArg1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSampleDataSet);
            this.Controls.Add(this.lblRequest);
            this.Controls.Add(this.lstSampleDataSet);
            this.Controls.Add(this.lstRequest);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBox1);
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Forte Server Test Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox lstRequest;
        private System.Windows.Forms.ListBox lstSampleDataSet;
        private System.Windows.Forms.Label lblRequest;
        private System.Windows.Forms.Label lblSampleDataSet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblArg1;
        private System.Windows.Forms.Label lblArg2;
        private System.Windows.Forms.Label lblArg3;
        private System.Windows.Forms.TextBox txtArg1;
        private System.Windows.Forms.TextBox txtArg2;
        private System.Windows.Forms.TextBox txtArg3;
    }
}

