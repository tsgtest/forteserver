﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Xml;
using System.Diagnostics;

namespace TestClient
{
    public partial class Form1 : Form
    {
        //Microsoft.Office.Interop.Word.Application word = null;
        object file = null;

        public Form1()
        {
            InitializeComponent();
        }

        private string ProcessInstruction(){
            try
            {
         /* <ForteInstruction type=CreateDocument>
	     *      <ID>3039</ID>
	     *      <Prefill>
                    <Authors>2143<\Authors>
                    <AuthorInitials>LAA</AuthorInitials>
                    <CC><FULLNAME Index='1'>Jerry Wolfe</FULLNAME></CC>
                    <BCC><FULLNAME Index='1'>Linda J. Sackett</FULLNAME></BCC>
                    <ClientMatterNumber>53001.001</ClientMatterNumber>
                    <ClosingPhrase>Sincerely,</ClosingPhrase>
                    <DateFormat>MMMM d, yyyy</DateFormat>
                    <DeliveryPhrases>By Certified Mail
                    Return Receipt Requested</DeliveryPhrases>
                    <Enclosures>Enclosure</Enclosures>
                    <HeaderDeliveryPhrases>True</HeaderDeliveryPhrases>
                    <IncludeAdmittedIn>False</IncludeAdmittedIn>
                    <IncludeEMail>True</IncludeEMail>
                    <IncludeFax>False</IncludeFax>
                    <IncludeName>True</IncludeName>
                    <IncludePhone>True</IncludePhone>
                    <LetterheadIncludeTitle>False</LetterheadIncludeTitle>
                    <Page2HeaderText>Ms. Laura Halliday
                    Ms. Valerie Melville</Page2HeaderText>
                    <ReLine>Subject line</ReLine>
                    <Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1'>Ms. Laura Halliday</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1'>Project Manager</TITLE><COMPANY Index='1'>Legal MacPac</COMPANY><COREADDRESS Index='1'>1430 Judah Street, #2
                    San Francisco, CA  94122</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2'>Ms. Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2'>Product and Client Services Manager</TITLE><COMPANY Index='2'>The Sackett Group, Inc.</COMPANY><COREADDRESS Index='2'>7367 Los Brazos
                    San Diego, CA  92127</COREADDRESS></Recipients>
                    <Salutation>Dear Laura and Valerie:</Salutation>
                    <AuthorName>Louis A. Armstrong</AuthorName>
                    <IncludeFirmName>True</IncludeFirmName>
                    <IncludeTitle>True</IncludeTitle>
                    <TypistInitials>ljs</TypistInitials>
         *      </Prefill>
	     *      <EmailAddress>DFisherman@thesackettgroup.com</EmailAddress>
	     *      <OutputFormat>docx</OutputFormat>
         * </ForteInstruction> */

                string x = this.Text;
                this.Text = "Processing.";
                this.richTextBox1.Text = "";

                //add client functionality to Word, which will allow users to select segment and prefill, then send to server
                TSG.Forte.ForteServer.ForteServerClient oServer = new TSG.Forte.ForteServer.ForteServerClient();

                string xPrefill1 = "<Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index=\"1\" UNID=\"0\">Doug Miller</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"1\" UNID=\"0\">Developer</TITLE><COMPANY Index=\"1\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"1\" UNID=\"0\">28 6th Ave. &#13;&#10;NY, NY 10003</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index=\"2\" UNID=\"0\">Linda Sackett</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"2\" UNID=\"0\">Operations Manager</TITLE><COMPANY Index=\"2\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"2\" UNID=\"0\">Her address &#13;&#10;Her city, state, and zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString><NewVariable1>900 Fifth Avenue, Suite 2500&#13;&#10;New York, New York  10003</NewVariable1></Recipients><NewVariable2>this is the variable 2 value</NewVariable2><Reline><Standard Format=\"0\" Default=\"Alternate Insurance\">This is the reline value</Standard><Special Format=\"0\" Default=\"Alternate Insurance\"><Label Index=\"1\">Claimant</Label><Value Index=\"1\">c</Value><Label Index=\"2\">Date of Claim</Label><Value Index=\"2\">d</Value><Label Index=\"3\">Company Name</Label><Value Index=\"3\">c</Value><Label Index=\"4\">Action Number</Label><Value Index=\"4\">a</Value></Special></Reline><AuthorInitials>DCF</AuthorInitials><ReLine2><Standard Format=\"0\" Default=\"Alternate Insurance\">reline 2  value akls;dfj ads ;ruwpqeo ruqoiewur o qeorpoie q r pqoiewur qw erpoiquweqe pqiweu pqo ewiuq pirqpeiuqper qepur qpweqeo r periuq ep qwpriu wrq er poe rqerpqiew rq ewroipqwueroipqi roiru</Standard></ReLine2><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";
                string xPrefill2 = "<IncludeTitle>true</IncludeTitle><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1' UNID='0'>Jeffrey</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1' UNID='0'>Developer</TITLE><COMPANY Index='1' UNID='0'>TSG</COMPANY><COREADDRESS Index='1' UNID='0'>my address&#13;&#10;my city, my state my zip</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2' UNID='0'>Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2' UNID='0'>Manager</TITLE><COMPANY Index='2' UNID='0'>TSG</COMPANY><COREADDRESS Index='2' UNID='0'>her address&#13;&#10;her city, her state her zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString></Recipients><UseTableFormat>false</UseTableFormat><Reline><Standard Format='0' Default='Alternate Insurance'>this is the reline</Standard><Special Format='0' Default='Alternate Insurance'><Label Index='1'>Claimant</Label><Value Index='1'>a</Value><Label Index='2'>Date of Claim</Label><Value Index='2'>b</Value><Label Index='3'>Company Name</Label><Value Index='3'>c</Value><Label Index='4'>Action Number</Label><Value Index='4'>d</Value></Special></Reline><Subject>This is the subject line</Subject><Agreed>true</Agreed><DateFormat>MMMM dd, yyyy</DateFormat><ExecuteonBlock>true</ExecuteonBlock><TitleProperty>doc title</TitleProperty><CustomTitle>custom doc title</CustomTitle><DocumentVariableValue>dv custom 1 var text</DocumentVariableValue><SetLanguage>true</SetLanguage><FontName>Calibri</FontName><FontSize>12.0</FontSize><BodyTextFirstLineIndent>1.0</BodyTextFirstLineIndent><BodyTextAlignment>0</BodyTextAlignment><AuthorName>Daniel C. Fisherman</AuthorName><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";
                string xID = null;
                string xPrefill = null;

                if (lstSampleDataSet.SelectedIndex == 0)
                {
                    xPrefill = xPrefill1;
                }
                else
                {
                    xPrefill = xPrefill2;
                }

                xPrefill = "<Prefill>" + xPrefill + "</Prefill>";
                xPrefill = ConvertXmlToDelimitedString(xPrefill);

                string strHostName = Dns.GetHostName();
                    Console.WriteLine("Local Machine's Host Name: " + strHostName);
                // Then using host name, get the IP address list..
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                string addr = "unknown";

                foreach (var ip in ipEntry.AddressList)
                {
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && ip.ToString().StartsWith("10."))
                    {
                        addr = strHostName + " (IP: "  + ip.ToString() + "; Doc ID: " + xID + ")";
                        break;
                    }
                }

                string instruction = null;
                //string user = "Daniel Fisherman";
                string user = System.Environment.UserName;

                switch (this.lstRequest.Text.ToLower())
                {
                    case "createdocument":
                        instruction = "<ForteInstructions><ForteInstruction type=\"CreateDocument\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
                     "</Prefill><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getfoldersxml":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetFoldersXml\"><User>" + user + "</User><ParentFolderID>" + this.txtArg1.Text +
                            "</ParentFolderID><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getfoldermembersjson":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetFoldersXml\"><User>" + user + "</User><ParentFolderID>" + this.txtArg1.Text +
                            "</ParentFolderID><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getuixml":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetUIXml\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
                     "</Prefill><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getuihtml":
                    case "getuihtml (native)":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetUIHtml\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
                     "</Prefill><Format>" + "0" + "</Format><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getuihtml (jstree)":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetUIHtml\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
                     "</Prefill><Format>" + "1" + "</Format><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getuihtml (fancytree)":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetUIHtml\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
                     "</Prefill><Format>" + "2" + "</Format><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getlistxml":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetListXml\"><User>" + user + "</User><ListName>" + this.txtArg1.Text + "</ListName>" +
                     "<EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "gethelptext":
                        string xHelp = "";
                        if (this.txtArg1.Text != "")
                        {
                            if (xHelp.IndexOf(@"file:///") == -1 && xHelp.IndexOf(@"file:\\\") == -1)
                            {
                                xHelp = @"file:///" + this.txtArg1.Text;
                            }
                            else
                            {
                                xHelp = this.txtArg1.Text;
                            }
                        }
                        else
                        {
                            xHelp = this.txtArg2.Text;
                        }
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetHelpText\"><User>" + user + "</User><HelpText>" + xHelp + "</HelpText>" +
                     "<EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    case "getexternaldataset":
                        instruction = "<ForteInstructions><ForteInstruction type=\"GetExternalDataSet\"><User>" + user + "</User><ID>" + this.txtArg1.Text + 
                            "</ID><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                        break;
                    default:
                        MessageBox.Show("Support for this request has yet to be implemented.  Coders, see Form1.ProcessInstruction().",
                            "Not implemented", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                        return null;
                }

                string[] aText = oServer.ProcessInstructionSet(instruction);

                this.Text = x;

                string xText = string.Join("\r\n\r\n", aText);
                this.richTextBox1.Text = xText;

                switch (this.lstRequest.Text.ToLower())
                {
                    case "createdocument":
                        string xFileName = null;
                        //string xDir = LMP.Registry.GetMacPac10Value("SaveDirectoryUrl");
                        string xDir = @"C:\Forte Server\Docs";

                        if (!System.IO.Directory.Exists(xDir))
                            System.IO.Directory.CreateDirectory(xDir);

                        //{
                        if (!xDir.EndsWith("\\"))
                        {
                            xDir = xDir + "\\";
                        }

                        xFileName = xDir + System.Guid.NewGuid().ToString() + ".docx";
                        //}
                        //else
                        //{
                        //    xFileName = @"C:\" + System.Guid.NewGuid().ToString() + ".docx";
                        //}

                        using (FileStream oFS = File.Create(xFileName))
                        {
                            byte[] b = Convert.FromBase64String(xText);
                            oFS.Write(b, 0, b.Length);
                        }
                        break;
                    case "getuihtml (native)":
                    case "getuihtml (jstree)":
                    case "getuihtml (fancytree)":
                        string xPath = Path.GetTempPath() + "\\";
                        string xFile = xPath + "Segment" + this.txtArg1.Text + ".html";
                        File.WriteAllText(xFile, xText);
                        if (File.Exists(xFile))
                        {
                            //launch new text file
                            Process oProcess = new Process();
                            oProcess.StartInfo.FileName = xFile;
                            oProcess.Start();

                            //oProcess.WaitForInputIdle();
                        }
                        break;
                    default:
                        break;
                }

                return null;
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message + "\r\n" + oE.StackTrace, "Error");
                return null;
            }
        }

        private string ConvertXmlToDelimitedString(string xPrefill)
        {
            StringBuilder oSB = new StringBuilder();

            XmlDocument oXml = new XmlDocument();
            oXml.LoadXml(xPrefill);

            XmlNodeList oElements = oXml.DocumentElement.ChildNodes;
            foreach (XmlNode element in oElements)
            {
                oSB.AppendFormat("{0}¤{1}Þ", element.Name, element.InnerXml);
            }

            oSB.Remove(oSB.Length - 1, 1);
            return oSB.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.lstSampleDataSet.Items.Add("Data Set 1");
            this.lstSampleDataSet.Items.Add("Data Set 2");

            this.lstRequest.SelectedIndex = 0;
            this.lstSampleDataSet.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                file = ProcessInstruction();
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //reset
                this.txtArg1.Text = "";
                this.txtArg2.Text = "";
                this.txtArg3.Text = "";
                this.lblArg1.Text = "Argument 1";
                this.lblArg2.Text = "Argument 2";
                this.lblArg3.Text = "Argument 3";

                switch (this.lstRequest.Text.ToLower())
                {
                    case "createdocument":
                    case "getuixml":
                    case "getuihtml":
                    case "getuihtml (native)":
                    case "getuihtml (jstree)":
                    case "getuihtml (fancytree)":
                    case "getfoldermembersjson":
                        this.lblArg1.Text = "Segment &ID:";
                        this.lblArg1.Enabled = true;
                        this.lblArg2.Enabled = false;
                        this.lblArg3.Enabled = false;
                        this.txtArg1.Enabled = true;
                        this.txtArg2.Enabled = false;
                        this.txtArg3.Enabled = false;
                        this.lstSampleDataSet.Enabled = true;
                        this.lblSampleDataSet.Enabled = true;
                        this.button2.Enabled = true;
                        break;
                    case "getfoldersxml":
                        this.lblArg1.Text = "Parent Folder &ID:";
                        this.lblArg1.Enabled = true;
                        this.lblArg2.Enabled = false;
                        this.lblArg3.Enabled = false;
                        this.txtArg1.Enabled = true;
                        this.txtArg2.Enabled = false;
                        this.txtArg3.Enabled = false;
                        this.lstSampleDataSet.Enabled = false;
                        this.lblSampleDataSet.Enabled = false;
                        this.button2.Enabled = true;
                       break;
                    case "getlistxml":
                        this.lblArg1.Text = "List Name:";
                        this.lblArg1.Enabled = true;
                        this.lblArg2.Enabled = false;
                        this.lblArg3.Enabled = false;
                        this.txtArg1.Enabled = true;
                        this.txtArg2.Enabled = false;
                        this.txtArg3.Enabled = false;
                        this.lstSampleDataSet.Enabled = false;
                        this.lblSampleDataSet.Enabled = false;
                        this.button2.Enabled = true;
                        break;
                    case "gethelptext":
                        this.lblArg1.Text = "File name:";
                        this.lblArg1.Enabled = true;
                        this.lblArg2.Enabled = true;
                        this.lblArg2.Text = "OR Help text:";
                        this.lblArg3.Enabled = false;
                        this.txtArg1.Enabled = true;
                        this.txtArg2.Enabled = true;
                        this.txtArg3.Enabled = false;
                        this.lstSampleDataSet.Enabled = false;
                        this.lblSampleDataSet.Enabled = false;
                        this.button2.Enabled = true;
                        break;
                    case "getexternaldataset":
                        this.lblArg1.Text = "Data Set ID";
                        this.lblArg1.Enabled = true;
                        this.lblArg2.Enabled = false;
                        this.lblArg3.Enabled = false;
                        this.txtArg1.Enabled = true;
                        this.txtArg2.Enabled = false;
                        this.txtArg3.Enabled = false;
                        this.lstSampleDataSet.Enabled = false;
                        this.lblSampleDataSet.Enabled = false;
                        this.button2.Enabled = true;
                        break;
                    default:
                        this.lstSampleDataSet.Enabled = false;
                        this.lblSampleDataSet.Enabled = false;
                        this.lblArg1.Enabled = false;
                        this.lblArg2.Enabled = false;
                        this.lblArg3.Enabled = false;
                        this.txtArg1.Enabled = false;
                        this.txtArg2.Enabled = false;
                        this.txtArg3.Enabled = false;
                        this.button2.Enabled = false;
                        MessageBox.Show("Support for this request has yet to be implemented.  Coders, see Form1.ProcessInstruction().",
                            "Not implemented", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                        return;
                }
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }
        }
    }
}
